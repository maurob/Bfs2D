MPI_HOME=/usr
CUDA_HOME=/opt/cuda-8.0

CC=$(MPI_HOME)/bin/mpicc
LD=$(CUDA_HOME)/bin/nvcc
CUDACC=$(CUDA_HOME)/bin/nvcc

CUDA_ARCH=-arch=sm_61
CUDACFLAGS=-m64 -c -O3 --ptxas-options=-v -DROWXTH=6 -DUSE_LDG -I./cub-1.5.1 #-D_LARGE_LVERTS_NUM #-D_TIMINGS_KERNEL

CFLAGS=-g -W -Wall -Wno-unused-function -Wno-unused-parameter -c -O3 -I $(CUDA_HOME)/include -Wno-deprecated-gpu-targets #-D_TIMINGS -D_LARGE_LVERTS_NUM #-D_TIMINGS_TRANSF
LDFLAGS = generator/libgraph_generator_mpi.a -L$(MPI_HOME)/lib64 -lmpi -L$(CUDA_HOME)/lib64 -lcudart -Wno-deprecated-gpu-targets
#LDFLAGS = -L/mpip.../lib -lmpiP -L/opt/cuda-5.5/lib64 -lcudart -lbfd -liberty -lunwind

OBJ=bfs2d.o cuda_kernels.o adtp.o

bfs2d: ${OBJ}
	${LD} -o bfs2d ${OBJ} ${LDFLAGS}

.c.o:; $(CC) $(CFLAGS) $< -o $@

%.o: %.cu
	$(CUDACC) $(CUDACFLAGS) $(CUDA_ARCH) $<
clean:
	rm -rf *.o bfs2d
