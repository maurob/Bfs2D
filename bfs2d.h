/* 
 * Copyright 2013-2016 Mauro Bisson <mauro.bis@gmail.com>
 * 		       Massimo Bernaschi <massimo.bernaschi@gmail.com>
 * 		       Enrico Mastrostefano <babil.babilon@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

#ifndef _BFS2D_H_
#define _BFS2D_H_

#ifdef	_LARGE_LVERTS_NUM
#define LOCINT	   unsigned long long int //uint64_t
#define LOCPRI	   "llu" //PRIu64
#define LOCINT_MPI MPI_UNSIGNED_LONG_LONG
#define LOCINT_SZ  (8)
#else
#define LOCINT	   uint32_t
#define LOCPRI	   PRIu32
#define LOCINT_MPI MPI_UNSIGNED
#define LOCINT_SZ  (4)
#endif

#define MAX(a,b)	(((a)>(b))?(a):(b))
#define MIN(a,b)	(((a)<(b))?(a):(b))

#define MAX_PROC_I	1024
#define MAX_PROC_J	1024
#define MAX_LINE	1024

#if 1
#define GI2PI(i)	(((i)%col_bl)/row_bl)		// global row -> processor row
#define GJ2PJ(j)	((j)/col_bl)			// global col -> processor col
#define EDGE2PROC(i,j)	((int)(GI2PI(i)*C + GJ2PJ(j)))	// global (i,j) -> processor
#define VERT2PROC(i)	(EDGE2PROC(i,i))		// global vertex (i,)  -> processor

#define GI2LOCI(i)	(((i)/col_bl)*row_bl + (i)%row_bl) // global row -> local row
#define GJ2LOCJ(j)	((j)%col_bl)			   // global col -> local col

#define LOCI2GI(i)	(((i)/row_bl)*col_bl + myrow*row_bl + (i)%row_bl) // local row -> global row
#define LOCJ2GJ(j)	(mycol*col_bl + (j))				  // local col -> global col

#define	LIJ2LN(i,j)	((i)*col_bl + (j))	    // local  edge (i,j) to  local linear index
#define	LIJ2GN(i,j)	(LOCI2GI(i)*N + LOCJ2GJ(j)) // local  edge (i,j) to global linear index
#define GIJ2GN(i,j)	((i)*N + (j))		    // global edge (i,j) to global linear index

#define	GN2GI(n)	((n)/N) // global linear index to global (i,)
#define	GN2GJ(n)	((n)%N) // global linear index to global (,j)

#define	GN2LI(n)	(GI2LOCI(GN2GI(n))) // global linear index to local (i,)
#define	GN2LJ(n)	(GJ2LOCJ(GN2GJ(n))) // global linear index to local (,j)

#define MYLOCI2LOCJ(i)	(((i)%row_bl) + myrow*row_bl)	// local OWNED row -> local col
#define REMJ2GJ(j,pj)	((pj)*col_bl + (j))		// col of proc (*,pj) -> global col

#define CUDA_MYLOCI2LOCJ(i)  ((i)+(dmyrow-((i)/drow_bl))*drow_bl)

#else

#define GI2PI(i)	((i)%R)				// global row -> processor row
#define GJ2PJ(j)	((j)/col_bl)  //((j)%C)		// global col -> processor col
#define EDGE2PROC(i,j)	((int)(GI2PI(i)*C + GJ2PJ(j)))	// global (i,j) -> processor
#define VERT2PROC(i)	(EDGE2PROC(i,i))		// global vertex (i,)  -> processor

#define GI2LOCI(i)	((i)/R)				// global row -> local row
#define GJ2LOCJ(j)	((j)%col_bl)	//((j)/C)	// global col -> local col

#define LOCI2GI(i)	((i)*R + myrow)	// local row -> global row
#define LOCJ2GJ(j)	(mycol*col_bl + (j)) //((j)*C + mycol)	// local col -> global col
#define MYLOCI2LOCJ(i)	(((i)%row_bl)*R + myrow) 		// local OWNED row -> local col
#define REMJ2GJ(j,pj)	((pj)*col_bl + (j))    //((j)*C + (pj))	// col of proc (*,pj) -> global col

#define CUDA_MYLOCI2LOCJ(i)  (((i)%drow_bl)*dR + dmyrow)

#endif ///////////////

#define BITS(ptr)	(sizeof(*ptr)<<3)
#define MSKGET(mask,n)  (mask[(n)/BITS(mask)] &  (1ULL<<((n)%BITS(mask)))) // get mask bit for vert at CSR index n
#define MSKSET(mask,n)  {mask[(n)/BITS(mask)] |= (1ULL<<((n)%BITS(mask)));} // set mask bit for vert at CSR index n

#if 0
#define TIMER_DEF(n)	 struct timeval temp_1_##n={0,0}, temp_2_##n={0,0}
#define TIMER_START(n)	 gettimeofday(&temp_1_##n, (struct timezone*)0)
#define TIMER_STOP(n)	 gettimeofday(&temp_2_##n, (struct timezone*)0)
#define TIMER_ELAPSED(n) ((temp_2_##n.tv_sec-temp_1_##n.tv_sec)*1.e6+(temp_2_##n.tv_usec-temp_1_##n.tv_usec))
#else
#define TIMER_DEF(n)	 double __tmp_time_##n=0.0
#define TIMER_START(n)	 __tmp_time_##n = MPI_Wtime()
#define TIMER_STOP(n)	 __tmp_time_##n = MPI_Wtime()-__tmp_time_##n
#define TIMER_ELAPSED(n) (__tmp_time_##n)
#endif

#ifdef __cplusplus
#define LINKAGE "C"
#else
#define LINKAGE
#endif

#define BU_SEARCH

#ifndef BU_SEARCH
extern LINKAGE void resetcuda_data(uint64_t ned, LOCINT *col, LOCINT *row);
#else
extern LINKAGE void resetcuda_data(uint64_t ned, LOCINT *col, LOCINT *row, LOCINT *csr_col, LOCINT *csr_row, LOCINT *csr_gdg);
#endif

extern LINKAGE int sort_graph(uint64_t **ed, uint64_t ned);
extern LINKAGE void *Realloc(void *ptr, size_t sz);

extern LINKAGE void initcuda();
extern LINKAGE size_t initcuda_data(uint64_t ned);
extern LINKAGE int assignDeviceToProcess();
extern LINKAGE void set_mlp_cuda(LOCINT row, int level, int pred);
extern LINKAGE void set_root_cuda(LOCINT root);

extern LINKAGE LOCINT scan_col_csc_cuda(LOCINT *rbuf, LOCINT ld,
					int *rnum, int R, int C,
					LOCINT *sbuf, int *snum,
					int level, int inmask, int outmask);
#ifdef BU_SEARCH
extern LINKAGE LOCINT scan_col_csc_bu_cuda(LOCINT *rbuf, LOCINT ld,
					   int *rnum, int R, int C,
					   LOCINT *sbuf, int *snum,
					   int level, int inmask, int outmask);
#endif
#if 0
// this computes the most convenient kernel to use
exterd_rbufn LINKAGE LOCINT scan_col_csc_cudaAAA(LOCINT *rbuf, LOCINT ld, int *rnum, int R, int C, LOCINT *sbuf, int *snum, int level, int inmask, int outmask);
#endif
extern LINKAGE LOCINT append_rows_cuda(LOCINT *rbuf, LOCINT ld, int *rnum,
				       int R, int C, LOCINT *frt, int level,
				       int iomask);
extern LINKAGE void get_prd(int *prd);
extern LINKAGE void get_lvl(int *lvl);
extern LINKAGE void get_msk(LOCINT *msk);
extern LINKAGE void fincuda();
extern LINKAGE void *Malloc(size_t sz);
extern LINKAGE uint64_t compact(uint64_t *v, uint64_t ld, int *vnum, int n);
extern LINKAGE void prstat(double val, const char *msg, int det);
extern LINKAGE void *CudaMallocHostSet(size_t size, int val);
extern LINKAGE void CudaFreeHost(void *ptr);
extern LINKAGE void pred_reqs_cuda(LOCINT min, LOCINT max,
				   LOCINT *sbuf, LOCINT ld, int *snum);
extern LINKAGE void pred_resp_cuda(LOCINT *rbuf, LOCINT ld, int *rnum, LOCINT *sbuf, int *snum);

extern uint64_t	N;	/* number of vertices: N */
extern LOCINT	row_bl; /* adjacency matrix rows per block: N/(RC) */
extern LOCINT	col_bl; /* adjacency matrix columns per block: N/C */
extern LOCINT	row_pp; /* adjacency matrix rows per proc: N/(RC) * C = N/R */

extern int C;
extern int R;
extern int myid;
extern int ntask;
extern int myrow;
extern int mycol;
extern int pmesh[MAX_PROC_I][MAX_PROC_J];
extern int bmaplen;

#define MAX_BFS_LEVEL (1024*100)
typedef struct {
	uint64_t nedges;
	int	 scanbu;
	int	 inmsk;
	int	 outmsk;
} lvldata_t;

typedef struct {
	double	vtrans;
	double	expand;
	double	htrans;
	double	update;
	double	allred;
	double	totalt;
	uint64_t nedges;
} bfsdata_t;

#endif
