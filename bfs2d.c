/* 
 * Copyright 2013-2016 Mauro Bisson <mauro.bis@gmail.com>
 * 		       Massimo Bernaschi <massimo.bernaschi@gmail.com>
 * 		       Enrico Mastrostefano <babil.babilon@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdarg.h>
#include <mpi.h>
#include <getopt.h>
#include <limits.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <math.h>
#include <sys/time.h>
#include <time.h>
//#include <cuda_profiler_api.h>

#include "bfs2d.h"
#include "generator/make_graph.h"

#define VTAG(t)  (  0*ntask+(t))
#define HTAG(t)  (100*ntask+(t))
#define PTAG(t)  (200*ntask+(t))
#define VTAG2(t) (300*ntask+(t))
#define HTAG2(t) (400*ntask+(t))

#define BM_THRESHOLD (1.0E-5)
#define BU_THRESHOLD (2.0E-2)

#define BM_TH_SEARCH_MIN (1.0E-5)
#define BM_TH_SEARCH_MAX (2.0E-4)

#define BU_TH_SEARCH_MIN (1.0E-2)
#define BU_TH_SEARCH_MAX (1.0E+0)

#define TO_MIN_MULTIPLE(x,y) (((x+((y)-1))/(y))*y)

//static FILE *gfp=NULL;

uint64_t N=0;	/* number of vertices: N */
LOCINT	row_bl; /* adjacency matrix rows per block: N/(RC) */
LOCINT	col_bl; /* adjacency matrix columns per block: N/C */
LOCINT	row_pp; /* adjacency matrix rows per proc: N/(RC) * C = N/R */

int C=1;
int R=1;
int myid;
int ntask;
int myrow;
int mycol;
int pmesh[MAX_PROC_I][MAX_PROC_J];
int bmaplen;

MPI_Comm Row_comm, Col_comm;

static void prexit(const char *fmt, ...) {

	int myid;
	va_list ap;

	va_start(ap, fmt);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	if (0 == myid) vfprintf(stderr, fmt, ap);
	MPI_Finalize();
	exit(EXIT_FAILURE);
}

void *Malloc(size_t sz) {

        void *ptr;

        ptr = (void *)malloc(sz);
        if (!ptr) {
                fprintf(stderr, "Cannot allocate %zu bytes...\n", sz);
                exit(EXIT_FAILURE);
        }
	memset(ptr, 0, sz);
        return ptr;
}

char *Strndup(const char *s, size_t n) {

	char *ptr;

	ptr = strndup(s, n);
	if (!ptr) {
		fprintf(stderr, "Cannot duplicate string...\n");
		exit(EXIT_FAILURE);
	}
	return ptr;
}

void prstat(double val, const char *msg, int det) {

	int	myid, ntask, i, j, w1, w2, min, max;
	double	t, *v = NULL;
	double	m, s;

	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	MPI_Comm_size(MPI_COMM_WORLD, &ntask);

	if (myid == 0)
		v = (double *)Malloc(ntask*sizeof(*v));

	MPI_Gather(&val, 1, MPI_DOUBLE, v, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	if (myid == 0) {

		t = 0;
		m = s = 0.0;
		min = max = 0;
		for(i = 0; i < ntask; i++) {
			if (v[i] < v[min]) min = i;
			if (v[i] > v[max]) max = i;
			t += v[i];
			m += v[i];
			s += v[i]*v[i];
		}
		m /= ntask;
		s = sqrt((1.0/(ntask-1))*(s-ntask*m*m));
		fprintf(stdout,
			"%s: avg=%.2lf, std=%.2lf%%, min(%d)=%.2lf, max(%d)=%.2lf, tot=%.2lf\n",
			msg, m, (s/m)*100.0, min, v[min], max, v[max], t);
		if (det) {	
			for(w1 = 0, val =  ntask; val; val /= 10, w1++);
			for(w2 = 0, val = v[max]; val; val /= 10, w2++);

			for(i = 0; i < R; i++) {
				fprintf(stdout, " ");
				for(j = 0; j < C; j++) {
					fprintf(stdout, "%*d: %*.2lf  ", w1, i*C+j, w2, v[i*C+j]);
				}
				fprintf(stdout, "\n");
			}
		}
		free(v);
	}
	return;
}

void *Realloc(void *ptr, size_t sz) {

        void *lp;

        lp = (void *)realloc(ptr, sz);
        if (!lp && sz) {
                fprintf(stderr, "Cannot reallocate to %zu bytes...\n", sz);
                exit(EXIT_FAILURE);
        }
        return lp;
}
	
static FILE *Fopen(const char *path, const char *mode) {

	FILE *fp = NULL;
	fp = fopen(path, mode);
	if (!fp) {
                fprintf(stderr, "Cannot open file %s...\n", path);
                exit(EXIT_FAILURE);
        }
	return fp;
}

static off_t get_fsize(const char *fpath) {

	struct stat	st;
	int		rv;

	rv = stat(fpath, &st);
	if (rv) {
		fprintf(stderr, "Cannot stat file %s...\n", fpath);
		exit(EXIT_FAILURE);
	}
	return st.st_size;
}

static uint64_t *mirror(uint64_t *ed, uint64_t *ned) {

	uint64_t i, n;
	
	ed = (uint64_t *)Realloc(ed, (ned[0]*4)*sizeof(*ed));

	n = 0;
	for(i = 0; i < ned[0]; i++) {
		if (ed[2*i] != ed[2*i+1]) {
			ed[2*ned[0]+2*n] = ed[2*i+1];
			ed[2*ned[0]+2*n+1] = ed[2*i];
			n++;
		}
	}
	ned[0] += n;
	return ed;
}

static uint64_t read_graph(int myid, int ntask, const char *fpath, uint64_t **edge, uint64_t *maxv) {
#define ALLOC_BLOCK	(1024*1024)

	uint64_t *ed=NULL;
	uint64_t i, j;
	uint64_t n, nmax;
	size_t	 size;
	int64_t	 off1, off2;

	int	 rem;
	FILE	 *fp;
	char	 str[MAX_LINE];

	*maxv = 0;

	size = get_fsize(fpath);
	rem = size % ntask;
	off1 = (size/ntask)* myid    + (( myid    > rem)?rem: myid);
	off2 = (size/ntask)*(myid+1) + (((myid+1) > rem)?rem:(myid+1));

	fp = Fopen(fpath, "r");
	if (myid < (ntask-1)) {
		fseek(fp, off2, SEEK_SET);
		fgets(str, MAX_LINE, fp);
		off2 = ftell(fp);
	}
	fseek(fp, off1, SEEK_SET);
	if (myid > 0) {
		fgets(str, MAX_LINE, fp);
		off1 = ftell(fp);
	}

	//fprintf(stdout, "Process %d off1=%lld off2=%lld size=%lld\n",
	//	myid, off1, off2, off2-off1);

	n = 0;
	nmax = ALLOC_BLOCK; // must be even
	ed = (uint64_t *)Malloc(nmax*sizeof(*ed));

	/* read edges from file */
	while (ftell(fp) < off2) {

		fgets(str, MAX_LINE, fp);

		char *ptr;
		for(ptr = str; (*ptr == ' ') || (*ptr == '\t'); ptr++);
		if (ptr[0] == '#') continue;

		sscanf(str, "%"PRIu64" %"PRIu64"\n", &i, &j);
/*
		if (i >= N || j >= N) {
			fprintf(stderr,
				"[%d] found invalid edge in %s for N=%"PRIu64": (%"PRIu64", %"PRIu64")\n",
				myid, fpath, N, i, j);
			exit(EXIT_FAILURE);
		}
*/
		*maxv = MAX(*maxv, MAX(i,j));

		if (n >= nmax) {
			nmax += ALLOC_BLOCK;
			ed = (uint64_t *)Realloc(ed, nmax*sizeof(*ed));
		}
		ed[n]   = i;
		ed[n+1] = j;
		n += 2;
	}
	fclose(fp);

	n /= 2; // number of ints -> number of edges
	*edge = ed;

	return n;
#undef ALLOC_BLOCK
}

static long int set_root_seed(long int *seed) {

	static int ftime=1;
	long int srseed;

	if (!seed) {
		struct timeval time;
		gettimeofday(&time, NULL);
		srseed = getpid()+time.tv_sec+time.tv_usec;
		MPI_Allreduce(MPI_IN_PLACE, &srseed, 1, MPI_LONG, MPI_SUM, MPI_COMM_WORLD);
	} else {
		srseed = *seed;
	}
	if (myid == 0 && ftime) {
		fprintf(stdout, "Generating root vertices sequence from seed: %ld\n", srseed);
		ftime = 0;
	}
	srand48(srseed+myid);

	return srseed;
}

static uint64_t get_root(LOCINT *col) {

	LOCINT	ld, gd;
	int64_t lv, gv;

	do {
		lv = lrand48();
		MPI_Allreduce(&lv, &gv, 1, MPI_UNSIGNED_LONG_LONG, MPI_SUM, MPI_COMM_WORLD);
		gv %= N;
		//if (myid == VERT2PROC(gv)) fprintf(stderr, "Bfs will start from processor %d\n", p);

		if (mycol == GJ2PJ(gv))	ld = col[GJ2LOCJ(gv)+1]-col[GJ2LOCJ(gv)];
		else			ld = 0;

		MPI_Allreduce(&ld, &gd, 1, LOCINT_MPI, MPI_SUM, MPI_COMM_WORLD);
		//if (myid == VERT2PROC(gv)) fprintf(stderr, "[%d] %"LOCPRI"\n", myid, gd);
	} while (!gd);
	//} while (gd < 2);

	return gv;
}

static uint64_t gen_graph(int scale, int edgef, uint64_t seed, uint64_t **ed) {

	uint64_t ned;
	double	 initiator[4] = {.57, .19, .19, .05};

	make_graph(scale, (((int64_t)1)<<scale)*edgef, seed, seed+1, initiator, (int64_t *)&ned, (int64_t **)ed);

	return ned;
}

static uint64_t part_graph(int myid, int ntask, uint64_t **ed, uint64_t nedge) {

	uint64_t i;

	uint64_t *s_ed=NULL;
	uint64_t *r_ed=*ed;

	uint64_t totrecv;

	uint64_t *soff=NULL;
	uint64_t *roff=NULL;
	uint64_t *send_n=NULL;
	uint64_t *recv_n=NULL;

	int *pmask=NULL;

	int n, p;	
	MPI_Status *status;
	MPI_Request *request;

	/* compute processor mask for edges */
	pmask = (int *)Malloc(nedge*sizeof(*pmask));
	send_n = (uint64_t *)Malloc(ntask*sizeof(*send_n));
	for(i = 0; i < nedge; i++) {
		pmask[i] = EDGE2PROC(r_ed[2*i], r_ed[2*i+1]);
		send_n[pmask[i]]++;
	}

	/* sort edges by owner process (recv_n is used as a tmp) */
	soff = (uint64_t *)Malloc(ntask*sizeof(*soff));
	soff[0] = 0;
	for(p = 1; p < ntask; p++)
		soff[p] = soff[p-1] + send_n[p-1];
	
	recv_n = (uint64_t *)Malloc(ntask*sizeof(*recv_n));
	memcpy(recv_n, soff, ntask*sizeof(*soff));

	s_ed = (uint64_t *)Malloc(2*nedge*sizeof(*s_ed));
	for(i = 0; i < nedge; i++) {
		s_ed[2*recv_n[pmask[i]]]   = r_ed[2*i];
		s_ed[2*recv_n[pmask[i]]+1] = r_ed[2*i+1];
		recv_n[pmask[i]]++;
	}

	/* to proc k must be send send_n[k] edges starting at s_ei[soff[k]] */
	MPI_Alltoall(send_n, 1, MPI_UNSIGNED_LONG_LONG, recv_n, 1, MPI_UNSIGNED_LONG_LONG, MPI_COMM_WORLD);

	if (send_n[myid] != recv_n[myid]) {
		fprintf(stderr, "[%d] Error in %s:%d\n", myid, __func__, __LINE__);
		exit(EXIT_FAILURE);
	}

	roff = (uint64_t *)Malloc(ntask*sizeof(*roff));
	roff[0] = 0;
	totrecv = recv_n[0];
	for(p = 1; p < ntask; p++) {
		totrecv += recv_n[p];
		roff[p] = roff[p-1] + recv_n[p-1];
	}
	r_ed = (uint64_t *)Realloc(r_ed, 2*totrecv*sizeof(*r_ed));

	status  = (MPI_Status  *)Malloc(ntask*sizeof(*status));
        request = (MPI_Request *)Malloc(ntask*sizeof(*request));

	for(p = 0, n = 0; p < ntask; p++) {
		if (recv_n[p] == 0 || p == myid) continue;
		MPI_Irecv(r_ed + 2*roff[p], 2*recv_n[p], MPI_UNSIGNED_LONG_LONG, p, PTAG(p), MPI_COMM_WORLD, request+n);
		n++;
	}

	memcpy(r_ed+2*roff[myid], s_ed+2*soff[myid], 2*send_n[myid]*sizeof(*s_ed));
	for(p = 0; p < ntask; p++) {
		if (send_n[p] == 0 || p == myid) continue;
		MPI_Send(s_ed + 2*soff[p], 2*send_n[p], MPI_UNSIGNED_LONG_LONG, p, PTAG(myid),   MPI_COMM_WORLD);
	}
	MPI_Waitall(n, request, status);

	free(s_ed);
	free(send_n);
	free(soff);
	free(roff);
	free(recv_n);
	free(pmask);
	free(status);
	free(request);

	*ed = r_ed;
	return totrecv;
}

static int cmpedge(const void *p1, const void *p2) {

        uint64_t *l1 = (uint64_t *)p1;
        uint64_t *l2 = (uint64_t *)p2;

	if (EDGE2PROC(l1[0], l1[1]) < EDGE2PROC(l2[0], l2[1])) return -1;
	if (EDGE2PROC(l1[0], l1[1]) > EDGE2PROC(l2[0], l2[1])) return  1;

	if (l1[0] < l2[0]) return -1;
	if (l1[0] > l2[0]) return  1;

	if (l1[1] < l2[1]) return -1;
        if (l1[1] > l2[1]) return  1;

        return 0;
}

static uint64_t norm_graph(uint64_t *ed, uint64_t ned, LOCINT *deg) {

	uint64_t l, n;

	if (ned == 0) return 0;
	
	// this assumes the EDGE2PROC comparisons in cmpedge() are not relevant
	int rv = sort_graph(&ed, ned); 
	if (!rv) {
		if (myid == 0)
			fprintf(stdout, "not enough dev mem, using host...");
		qsort(ed, ned, sizeof(uint64_t[2]), cmpedge);
	}
	// record degrees considering multiple edges
	// and self-loop and remove them from edge list
	deg[GI2LOCI(ed[0])]++;
	for(n = l = 1; n < ned; n++) {

		deg[GI2LOCI(ed[2*n])]++;
		if (((ed[2*n]   != ed[2*(n-1)]  )  ||
		     (ed[2*n+1] != ed[2*(n-1)+1])) &&
		     (ed[2*n] != ed[2*n+1])) {

			ed[2*l]   = ed[2*n];
			ed[2*l+1] = ed[2*n+1];
			l++;
		}
	}
	return l;
}

#ifndef _LARGE_LVERTS_NUM
static int verify_32bit_fit(uint64_t *ed, uint64_t ned) {

	uint64_t i;

	for(i = 0; i < ned; i++) {
		uint64_t v;

		v = GI2LOCI(ed[2*i]);
		if (v >> (sizeof(LOCINT)*8)) {
			fprintf(stdout, "[%d] %"PRIu64"=GI2LOCI(%"PRIu64") won't fit in a 32-bit word\n", myid, v, ed[2*i]);
			return 0;
		}
		v = GJ2LOCJ(ed[2*i+1]);
		if (v >> (sizeof(LOCINT)*8)) {
			fprintf(stdout, "[%d] %"PRIu64"=GJ2LOCJ(%"PRIu64") won't fit in a 32-bit word\n", myid, v, ed[2*i+1]);
			return 0;
		}
	}
	return 1;
}
#endif

static void build_csc(uint64_t *ed, uint64_t ned, LOCINT **col, LOCINT **row) {

	LOCINT *r, *c, *tmp, i;

	/* count edges per col */
	tmp = (LOCINT *)Malloc(col_bl*sizeof(*tmp));
	for(i = 0; i < ned; i++)
		tmp[GJ2LOCJ(ed[2*i+1])]++;

	/* compute csc col[] vector with nnz in last element */
	c = (LOCINT *)Malloc((col_bl+1)*sizeof(*c));
	c[0] = 0;
	for(i = 1; i <= col_bl; i++)
		c[i] = c[i-1] + tmp[i-1];

	/* fill csc row[] vector */
	memcpy(tmp, c, col_bl*sizeof(*c)); /* no need to copy last int (nnz) */

	r = (LOCINT *)Malloc(ned*sizeof(*r));
	for(i = 0; i < ned; i++) {
		r[tmp[GJ2LOCJ(ed[2*i+1])]] = GI2LOCI(ed[2*i]);
		tmp[GJ2LOCJ(ed[2*i+1])]++;
	}
	free(tmp);

	*row = r;
	*col = c;

	return;
}

static void build_csr(uint64_t *ed, uint64_t ned, LOCINT **col, LOCINT **row, LOCINT **deg) {

	LOCINT *r, *c, *d, *tmp, i;

	/* count edges per row */
	tmp = (LOCINT *)Malloc(row_pp*sizeof(*tmp));
	for(i = 0; i < ned; i++)
		tmp[GI2LOCI(ed[2*i])]++;

	/* compute csc col[] vector with nnz in last element */
	r = (LOCINT *)Malloc((row_pp+1)*sizeof(*r));
	r[0] = 0;
	for(i = 1; i <= row_pp; i++)
		r[i] = r[i-1] + tmp[i-1];
	
	d = (LOCINT *)Malloc(row_pp*sizeof(*d));
	for(i = 0; i < row_pp; i++)
		d[i] = r[i+1]-r[i];

	/* fill csc row[] vector */
	memcpy(tmp, r, row_pp*sizeof(*tmp)); /* no need to copy last int (nnz) */

	c = (LOCINT *)Malloc(ned*sizeof(*c));
	for(i = 0; i < ned; i++) {
		c[tmp[GI2LOCI(ed[2*i])]] = GJ2LOCJ(ed[2*i+1]);
		tmp[GI2LOCI(ed[2*i])]++;
	}
	free(tmp);

	*row = r;
	*col = c;
	*deg = d;

	return;
}

static void check_recv_rows(LOCINT *rbuf, LOCINT ld, int *rnum, int n) {

	int	myid;
	int64_t i, j;

	MPI_Comm_rank(MPI_COMM_WORLD, &myid);

	/* sanity check */
	for(i = 0; i < n; i++) {
		for(j = 0; j < rnum[i]; j++) {

			if (rbuf[i*ld+j] < mycol*row_bl || rbuf[i*ld+j] >= (mycol*row_bl+row_bl)) {
				fprintf(stderr,
					"[%d] H-Received vertex %"LOCPRI" whose row does not belong to me!\n",
					myid, rbuf[i*ld+j]);
				exit(EXIT_FAILURE);
			}
		}
	}
	return;
}

static inline void exchange_vert4(LOCINT *frt, int nfrt,
				  LOCINT *rbuf, LOCINT ld, int *rnum,
				  MPI_Request *request, MPI_Status *status,
				  int post) {
	int i, p;
	static int ftime=1;
#ifdef _TIMINGS_TRANSF
	TIMER_DEF(0);
	MPI_Barrier(MPI_COMM_WORLD);
	TIMER_START(0);
#endif
	if (ftime) {
		for(i = 1; i < R; i++) {
			p = (myrow+i)%R;
			MPI_Irecv(rbuf + p*ld, ld, LOCINT_MPI,
				  pmesh[p][mycol], VTAG(pmesh[p][mycol]),
				  MPI_COMM_WORLD, request+i-1);
		}
		ftime = 0;
	}
	memcpy(rbuf+myrow*ld, frt, nfrt*sizeof(*frt));
	rnum[myrow] = nfrt;

	for(i = 1; i < R; i++) {
		p = (myrow+i)%R;
		MPI_Send(frt, nfrt, LOCINT_MPI,
			 pmesh[p][mycol], VTAG(myid), MPI_COMM_WORLD);
		//fprintf(gfp, "VN %d %d %d %d\n", pmesh[p][mycol], p, mycol, nfrt);
	}
	MPI_Waitall(R-1, request, status);
	for(i = 1; i < R; i++)
		MPI_Get_count(status+i-1, LOCINT_MPI, rnum+(myrow+i)%R);

	if (post) {
		for(i = 1; i < R; i++) {
			p = (myrow+i)%R;
			MPI_Irecv(rbuf + p*ld, ld, LOCINT_MPI,
				  pmesh[p][mycol], VTAG(pmesh[p][mycol]),
				  MPI_COMM_WORLD, request+i-1);
		}
	} else {
		ftime = 1;
	}
#ifdef _TIMINGS_TRANSF
	TIMER_STOP(0);
	prstat(TIMER_ELAPSED(0), "\nexchange_vert time (us)", 1);
	uint64_t bytes=R*nfrt*sizeof(*frt);
	prstat((double)bytes, "exchange_vert sent bytes", 1);
#endif
	return;
}

static inline void exchange_vert4_bmap(LOCINT *rbuf, LOCINT ld,
				       MPI_Request *request, MPI_Status *status,
				       int post) {
	int i, p;
	static int ftime=1;
#ifdef _TIMINGS_TRANSF
	TIMER_DEF(0);
	MPI_Barrier(MPI_COMM_WORLD);
	TIMER_START(0);
#endif
	if (ftime) {
		for(i = 1; i < R; i++) {
			p = (myrow+i)%R;
			MPI_Irecv(rbuf + p*ld, ld, LOCINT_MPI,
				  pmesh[p][mycol], VTAG2(pmesh[p][mycol]),
				  MPI_COMM_WORLD, request+i-1);
		}
		ftime = 0;
	}
	for(i = 1; i < R; i++) {
		p = (myrow+i)%R;
		MPI_Send(rbuf+myrow*ld, ld, LOCINT_MPI,
			 pmesh[p][mycol], VTAG2(myid), MPI_COMM_WORLD);
		//fprintf(gfp, "VB %d %d %d %d\n", pmesh[p][mycol], p, mycol, ld);
	}
	MPI_Waitall(R-1, request, status);
	if (post) {
		for(i = 1; i < R; i++) {
			p = (myrow+i)%R;
			MPI_Irecv(rbuf + p*ld, ld, LOCINT_MPI,
				  pmesh[p][mycol], VTAG2(pmesh[p][mycol]),
				  MPI_COMM_WORLD, request+i-1);
		}
	} else {
		ftime = 1;
	}
#ifdef _TIMINGS_TRANSF
	TIMER_STOP(0);
	prstat(TIMER_ELAPSED(0), "\nexchange_vert time (us)", 1);
	uint64_t bytes=R*ld*sizeof(*rbuf);
	prstat((double)bytes, "exchange_vert sent bytes", 1);
#endif
	return;
}

static inline void cancel_vert(MPI_Request *request) {

	int i;

	for(i = 1; i < R; i++)
		MPI_Cancel(request+i-1);

	MPI_Waitall(R-1, request, MPI_STATUSES_IGNORE);
	return;
}

static inline void exchange_horiz4(LOCINT *sbuf, LOCINT sld, int *snum,
				   LOCINT *rbuf, LOCINT rld, int *rnum,
				   MPI_Request *request, MPI_Status *status,
				   int post) {
	int i, p;
	static int ftime=1;
#ifdef _TIMINGS_TRANSF
	TIMER_DEF(0);
	MPI_Barrier(MPI_COMM_WORLD);
	TIMER_START(0);
#endif
	if (ftime) {
		for(i = 1; i < C; i++) {
			p = (mycol+i)%C;
			MPI_Irecv(rbuf + p*rld, rld, LOCINT_MPI,
				  pmesh[myrow][p], HTAG(pmesh[myrow][p]),
				  MPI_COMM_WORLD, request+i-1);
		}
		ftime = 0;
	}
	memcpy(rbuf+mycol*rld, sbuf+mycol*sld, snum[mycol]*sizeof(*sbuf));
	rnum[mycol] = snum[mycol];

	for(i = 1; i < C; i++) {
		p = (mycol+i)%C;
		MPI_Send(sbuf + p*sld, snum[p], LOCINT_MPI,
			 pmesh[myrow][p], HTAG(myid), MPI_COMM_WORLD);
		//fprintf(gfp, "HN %d %d %d %d\n", pmesh[myrow][p], myrow, p, snum[p]);
	}
	MPI_Waitall(C-1, request, status);
	for(i = 1; i < C; i++)
		MPI_Get_count(status+i-1, LOCINT_MPI, rnum+(mycol+i)%C);

	if (post) {	
		for(i = 1; i < C; i++) {
			p = (mycol+i)%C;
			MPI_Irecv(rbuf + p*rld, rld, LOCINT_MPI,
				  pmesh[myrow][p], HTAG(pmesh[myrow][p]),
				  MPI_COMM_WORLD, request+i-1);
		}
	} else {
                ftime = 1;
        }
#ifdef _TIMINGS_TRANSF
	TIMER_STOP(0);
	prstat(TIMER_ELAPSED(0), "exchange_horiz time (us)", 1);
	uint64_t bytes=0;
        for(i = 0; i < C; i++) bytes += snum[i]*sizeof(*sbuf);
        prstat((double)bytes, "exchange_horiz sent bytes", 1);
#endif
	return;
}

static inline void exchange_horiz4_bmap(LOCINT *sbuf, LOCINT *rbuf, LOCINT ld,
					MPI_Request *request, MPI_Status *status,
					int post) {
	int i, p;
	static int ftime=1;
#ifdef _TIMINGS_TRANSF
	TIMER_DEF(0);
	MPI_Barrier(MPI_COMM_WORLD);
	TIMER_START(0);
#endif
	if (ftime) {
		for(i = 1; i < C; i++) {
			p = (mycol+i)%C;
			MPI_Irecv(rbuf + p*ld, ld, LOCINT_MPI,
				  pmesh[myrow][p], HTAG2(pmesh[myrow][p]),
				  MPI_COMM_WORLD, request+i-1);
		}
		ftime = 0;
	}

	memset(rbuf+mycol*ld, 0, ld*sizeof(*rbuf));
	for(i = 1; i < C; i++) {
		p = (mycol+i)%C;
		MPI_Send(sbuf + p*ld, ld, LOCINT_MPI,
			 pmesh[myrow][p], HTAG2(myid), MPI_COMM_WORLD);
		//fprintf(gfp, "HB %d %d %d %d\n", pmesh[myrow][p], myrow, p, ld);
	}
	MPI_Waitall(C-1, request, status);
	if (post) {	
		for(i = 1; i < C; i++) {
			p = (mycol+i)%C;
			MPI_Irecv(rbuf + p*ld, ld, LOCINT_MPI,
				  pmesh[myrow][p], HTAG2(pmesh[myrow][p]),
				  MPI_COMM_WORLD, request+i-1);
		}
	} else {
                ftime = 1;
        }
#ifdef _TIMINGS_TRANSF
	TIMER_STOP(0);
	prstat(TIMER_ELAPSED(0), "exchange_horiz time (us)", 1);
	uint64_t bytes=C*ld*sizeof(*sbuf);
        prstat((double)bytes, "exchange_horiz sent bytes", 1);
#endif
	return;
}

static inline void cancel_horiz(MPI_Request *request) {

	int i;

	for(i = 1; i < C; i++)
		MPI_Cancel(request+i-1);

	MPI_Waitall(C-1, request, MPI_STATUSES_IGNORE);
	return;
}

static int cmpuloc(const void *p1, const void *p2) {

	LOCINT l1 = *(LOCINT *)p1;
	LOCINT l2 = *(LOCINT *)p2;

	if (l1 < l2) return -1;
	if (l1 > l2) return  1;
	return 0;
}

static void dump_lvl(int *lvl, LOCINT min, LOCINT max) {

	FILE	 *fp=NULL;
	char	 name[MAX_LINE];
	int	 myid;
	uint64_t i;

	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	snprintf(name, MAX_LINE, "lvl_%d", myid);
	fp = Fopen(name, "w");

	for(i = min; i < max; i++)
		fprintf(fp, "%"PRIu64" %d\n", LOCI2GI(i), lvl[i]);

	fclose(fp);
	return;
}

static void dump_prd(int *prd, LOCINT min, LOCINT max,
		     LOCINT *rbuf, LOCINT ld, int *rnum,
		     LOCINT *msk) {

	FILE	 *fp=NULL;
	char	 name[MAX_LINE];
	int	 myid;
	uint64_t i;

	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	snprintf(name, MAX_LINE, "prd_%d", myid);
	fp = Fopen(name, "w");

	memset(rnum, 0, C*sizeof(*rnum));
	for(i = min; i < max; i++) {
		int64_t pred;

		if	(!MSKGET(msk,i)) pred = -1;
		else if (prd[i] >= 0) 	 pred = LOCJ2GJ(prd[i]);
		else {
			int p = -(prd[i]+1);
			pred = REMJ2GJ(rbuf[p*ld + rnum[p]], p);
			rnum[p]++;
		}
		fprintf(fp, "%"PRIu64" %"PRId64"\n", LOCI2GI(i), pred);
	}
	fclose(fp);
	return;
}

static void dump_graph(const char *basename, uint64_t ned, uint64_t *edge) {

	int myid;
	char *fname;
	uint64_t i;
	FILE *fp;

	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
        fname = (char *)Malloc(MAX_LINE*sizeof(char));

        snprintf(fname, MAX_LINE, "%s_%d", basename, myid);

	fp = Fopen(fname, "w");
        for(i = 0; i < ned; i++)
                fprintf(fp, "%"PRIu64" %"PRIu64"\n", edge[2*i], edge[2*i+1]);
        fclose(fp);
        free(fname);

	return;
}

/*
static void bu_eur(double bu_thr, int level,
		   uint64_t nfrt, uint64_t ntot,
		   int *bu_lmin, int *bu_lmax) {
	
	//if (((double)nfrt)/row_pp > bu_thr) {
	if (((double)ntot)/N > bu_thr) {
		*bu_lmin = level+1;
		*bu_lmax = level+2;
	}
	return;
}
*/

static void bm_eur(double bm_thr, int level,
		   uint64_t nfrt, uint64_t ntot,
		   int *bm_lmin, int *bm_lmax) {
	
	if (((double)ntot)/N > bm_thr) {
		*bm_lmin = level+1;
		*bm_lmax = level+3;
	}
	return;
}

static int bfs(int undir, LOCINT *row, LOCINT *col, LOCINT *frt,
	       LOCINT *msk, int *lvl, int *prd, LOCINT *deg, uint64_t v0,
	       LOCINT *vRbuf, LOCINT *vRbuf2, int *vRnum,
	       LOCINT *hSbuf, LOCINT *hSbuf2, int *hSnum,
	       LOCINT *hRbuf, LOCINT *hRbuf2, int *hRnum,
	       MPI_Request *vrequest, MPI_Request *vrequest2,
	       MPI_Request *hrequest, MPI_Request *hrequest2,
	       double bm_thr, int bm_lvl_min, int bm_lvl_max,
	       double bu_thr, int bu_lvl_min, int bu_lvl_max,
	       MPI_Status *status, bfsdata_t *bfsdata,
	       lvldata_t *lvldata, int dump) {

	int 	 level;
	uint64_t n, ned, nfrt=0;

	TIMER_DEF(0);
#ifdef _TIMINGS
	TIMER_DEF(1);

	bfsdata->vtrans = 0;
	bfsdata->expand = 0;
	bfsdata->htrans = 0;
	bfsdata->update = 0;
	bfsdata->allred = 0;
#endif
	double total_time=0;

	if (VERT2PROC(v0) == myid) {

		LOCINT lv0 = GI2LOCI(v0);
		lvl[lv0] = 0;	
		MSKSET(msk,lv0);
		if (R > 1) {
			frt[nfrt++] = MYLOCI2LOCJ(lv0);
		} else {
			set_root_cuda(MYLOCI2LOCJ(lv0));
			nfrt = 1;
		}
		prd[lv0] = MYLOCI2LOCJ(lv0);
		set_mlp_cuda(lv0, 0, MYLOCI2LOCJ(lv0));
	}

	int nunvisit=row_pp;

	// START BFS
	MPI_Barrier(MPI_COMM_WORLD);
	TIMER_START(0);
	level = 1;
	//cudaProfilerStart();
	while(1) {

		int  inmsk = level >  bm_lvl_min && level <= bm_lvl_max;
		int outmsk = level >= bm_lvl_min && level <  bm_lvl_max;

		int scanbu;
		if (bu_lvl_min > -1) { 
			scanbu = level >= bu_lvl_min && level <= bu_lvl_max;
		} else {
			scanbu = nfrt/((double)nunvisit) > bu_thr;
		}

		if (R > 1) {
#ifdef _TIMINGS
			TIMER_START(1);
#endif
			// col-send frt
			// col-recv frts in vRbuf[0:R-1]
			if (!inmsk)
				exchange_vert4(frt, nfrt,
					      vRbuf, row_bl, vRnum,
					      vrequest, status, 1);
			else
				exchange_vert4_bmap(vRbuf2, bmaplen,
						    vrequest2, status, 1);
#ifdef _TIMINGS
			TIMER_STOP(1);
			bfsdata->vtrans += TIMER_ELAPSED(1);
#endif
		} else {
			vRnum[0] = nfrt;
		}
#ifdef _TIMINGS
		TIMER_START(1);
#endif
		// search for rows in CSR/CSC using vRbuf elements as cols,
		// and put non-masked rows hSbuf[0:C-1] (update mask, lvl and prd)
		if (scanbu) {
			scan_col_csc_bu_cuda(inmsk ? vRbuf2 : vRbuf,
					     row_bl, vRnum, R, C,
					     outmsk ? hSbuf2 : hSbuf,
					     hSnum, level, inmsk, outmsk);
		} else {
			scan_col_csc_cuda(inmsk ? vRbuf2 : vRbuf,
					  row_bl, vRnum, R, C,
					  outmsk ? hSbuf2 : hSbuf,
					  hSnum, level, inmsk, outmsk);
		}
#ifdef _TIMINGS
		TIMER_STOP(1);
		bfsdata->expand += TIMER_ELAPSED(1);
#endif
		if (C > 1) {
#ifdef _TIMINGS
			TIMER_START(1);
#endif
			// row-send hSbuf[0:C-1]
			// row-recv hRbuf[0:C-1]
			if (!outmsk)
				exchange_horiz4(hSbuf, row_bl, hSnum,
						hRbuf, row_bl, hRnum,
						hrequest, status, 1);
			else
				exchange_horiz4_bmap(hSbuf2, hRbuf2, bmaplen,
						     hrequest2, status, 1);
			//exchange_horiz_coll(hSbuf, row_bl, hSnum, hRbuf, row_bl, hRnum);
			//check_recv_rows(hRbuf, row_bl, hRnum, C);
#ifdef _TIMINGS
			TIMER_STOP(1);
			bfsdata->htrans += TIMER_ELAPSED(1);	
#endif
		}
#ifdef _TIMINGS
		TIMER_START(1);
#endif
		// append into the frontier (once), rows that are 
		// not masked. Level pred is updated for such rows.
		if (!outmsk)
			nfrt = append_rows_cuda(hRbuf, row_bl, hRnum, R, C, frt, level, 0);
		else
			nfrt = append_rows_cuda(hRbuf2, row_bl, hRnum, R, C, vRbuf2+myrow*bmaplen, level, 1);
#ifdef _TIMINGS
		TIMER_STOP(1);
		bfsdata->update += TIMER_ELAPSED(1);
		TIMER_START(1);
#endif
		if (ntask > 1) {
			MPI_Allreduce(&nfrt, &n, 1, MPI_UNSIGNED_LONG_LONG, MPI_SUM, MPI_COMM_WORLD);
		} else {
			n = nfrt;
		}
#ifdef _TIMINGS
		TIMER_STOP(1);
		bfsdata->allred += TIMER_ELAPSED(1);
#endif
		if (lvldata && level <= MAX_BFS_LEVEL) {
			lvldata[level-1].nedges = n;
			lvldata[level-1].scanbu = scanbu;
			lvldata[level-1].inmsk = inmsk;
			lvldata[level-1].outmsk = outmsk;
		}
		if (!n) break;

		nunvisit -= nfrt;
		/*
		if (bu_lvl_min == -1 && bu_lvl_max == -1 && level > 1) {
			bu_eur(bu_thr, level, nfrt, n, &bu_lvl_min, &bu_lvl_max);
		}
		*/
		if (bm_lvl_min == -1 && bm_lvl_max == -1) {
			bm_eur(bm_thr, level, nfrt, n, &bm_lvl_min, &bm_lvl_max);
		}

		level++;
	}
	//cudaProfilerStop();
	TIMER_STOP(0);
	total_time = TIMER_ELAPSED(0);
	bfsdata->totalt = total_time;

	get_msk(msk);
	if (dump) {
		// gather predecessor data into owner procs
		// (for output file parsing convenience)
#ifdef _TIMINGS
		TIMER_START(1);
#endif
		pred_reqs_cuda(mycol*row_bl, mycol*row_bl+row_bl, hSbuf, row_bl, hSnum);
		int i;
		for(i = 0; i < C; i++)
			qsort(hSbuf+i*row_bl, hSnum[i], sizeof(*hSbuf), cmpuloc);
		exchange_horiz4(hSbuf, row_bl, hSnum,
			       hRbuf, row_bl, hRnum,
			       hrequest, status, 1);
		pred_resp_cuda(hRbuf, row_bl, hRnum, hSbuf, hSnum);
		exchange_horiz4(hSbuf, row_bl, hSnum,
			       hRbuf, row_bl, hRnum,
			       hrequest, status, 1);
#ifdef _TIMINGS
		TIMER_STOP(1);
		//prstat((uint64_t)TIMER_ELAPSED(1), "\npredecessor scattering (us)", 0);
#endif
#if 0
		// sanity check
		for(i = 0; i < C; i++) {
			if (hRnum[i] != hSnum[i]) {
				fprintf(stderr,
					"[%d] rcvd more predecessor responses than requests from proc %d=(%d,%d)\n",
					myid, pmesh[myrow][i], myrow, (int)i);
				exit(EXIT_FAILURE);
			}
		}
#endif
		get_prd(prd);
		get_lvl(lvl);
		dump_lvl(lvl, mycol*row_bl, mycol*row_bl+row_bl);
		dump_prd(prd, mycol*row_bl, mycol*row_bl+row_bl, hRbuf, row_bl, hRnum, msk);
	}

	// compute teps
	n = 0;
	LOCINT j;
	for(j = 0; j < row_pp; j++)
		n += (!!MSKGET(msk,j)) * deg[j];

 	MPI_Reduce(&n, &ned, 1, MPI_UNSIGNED_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
	if (undir) ned >>= 1;
	bfsdata->nedges = ned;

	return level;
}

enum {MIN, FIRSTQ, MEDIAN, THIRDQ, MAX, MEAN, STD, HMEAN, HSTD, NSTAT};

static int dcmp(const void *a, const void *b) {

	const double da = *(const double*)a;
	const double db = *(const double*)b;

	if (da > db) return 1;
	if (db > da) return -1;
	if (da == db) return 0;
	
	fprintf (stderr, "No NaNs permitted in output.\n");
	abort ();
	
	return 0;
}

static void statsG500(double *out, double *data, int64_t n) {

	long double s, mean;
	double t;
	int k;

	/* Quartiles */
	qsort (data, n, sizeof (*data), dcmp);
	out[MIN] = data[0];
	
	t = (n+1) / 4.0;
	k = (int) t;
	if (t == k) out[FIRSTQ] = data[k];
	else	    out[FIRSTQ] = 3*(data[k]/4.0) + data[k+1]/4.0;

	t = (n+1) / 2.0;
	k = (int) t;
	if (t == k) out[MEDIAN] = data[k];
	else	    out[MEDIAN] = data[k]/2.0 + data[k+1]/2.0;

	t = 3*((n+1) / 4.0);
	k = (int) t;
	if (t == k) out[THIRDQ] = data[k];
	else	    out[THIRDQ] = data[k]/4.0 + 3*(data[k+1]/4.0);

	out[MAX] = data[n-1];

	s = data[n-1];
	for (k = n-1; k > 0; --k)
		s += data[k-1];
	mean = s/n;
	out[MEAN] = mean;

	s = data[n-1] - mean;
	s *= s;
	for (k = n-1; k > 0; --k) {
		long double tmp = data[k-1] - mean;
		s += tmp * tmp;
	}
	out[STD] = sqrt (s/(n-1));

	s = (data[0]? 1.0L/data[0] : 0);
	for (k = 1; k < n; ++k)
		s += (data[k]? 1.0L/data[k] : 0);
	out[HMEAN] = n/s;

	/*
		Nilan Norris, The Standard Errors of the Geometric and Harmonic
		Means and Their Application to Index Numbers, 1940.
		http://www.jstor.org/stable/2235723
	*/
	mean = s/n;
	s = (data[0]? 1.0L/data[0] : 0) - mean;
	s *= s;
	for (k = 1; k < n; ++k) {
		long double tmp = (data[k]? 1.0L/data[k] : 0) - mean;
		s += tmp * tmp;
	}
	s = (sqrt (s)/(n-1)) * out[HMEAN] * out[HMEAN];
	out[HSTD] = s;
}

#define PRINT_STATS(lbl, israte)					\
	do {								\
		fprintf(stdout, "%s statistics:\n", lbl);		\
		if (!israte) {						\
			printf("\t         mean: %lf\n", __sts[5]);	\
			printf("\t        stdev: %lf\n", __sts[6]);	\
		} else {						\
			printf("\t         mean: %lf\n", __sts[5]);	\
			printf("\t    harm mean: %lf\n", __sts[7]);	\
			printf("\t   harm stdev: %lf\n", __sts[8]);	\
		}							\
		printf("\t       median: %lf\n", __sts[2]);		\
		printf("\t      minimum: %lf\n", __sts[0]);		\
		printf("\t      maximum: %lf\n", __sts[4]);		\
		printf("\tfirstquartile: %lf\n", __sts[1]);		\
		printf("\tthirdquartile: %lf\n", __sts[3]);		\
	} while (0)

static void print_statsG500(const bfsdata_t *run_data, const int n) {

	int k;
	double *__tmp;
	double *__sts;

	__tmp = (double *)Malloc(n*sizeof (*__tmp));
	__sts = (double *)Malloc(NSTAT*sizeof (*__sts));
	if (!__tmp || !__sts) {
		fprintf(stderr, "Error allocating within final statistics calculation.");
		exit(EXIT_FAILURE);
	}

	for (k = 0; k < n; k++) {
		__tmp[k] = run_data[k].totalt;
	}
	statsG500(__sts, __tmp, n);
	PRINT_STATS("\nElapsed time (secs)", 0);

	for (k = 0; k < n; k++) {
		__tmp[k] = run_data[k].nedges;
	}
	statsG500(__sts, __tmp, n);
	PRINT_STATS("\nVisited edges", 0);

	for (k = 0; k < n; k++)
		__tmp[k] = run_data[k].nedges/run_data[k].totalt;
	statsG500(__sts, __tmp, n);
	PRINT_STATS("\nTEPS", 1);

	free(__tmp);
	free(__sts);

	return;
}

static void usage(const char *pname) {
	
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if (rank == 0) {
		const char *bname = rindex(pname, '/');
		if (!bname) bname = pname;
		else	    bname++;

		fprintf(stdout, 
			"Usage:\n"
			"\n"
			"%s [general options] [file input options]\n"
			"%s [general options] [RMAT input options]\n"
			"\n"
			"General options:\n"
			"\t-p <R>x<C>\n"
			"\t\tSpecifies the size of the bidimensional grid processes  rearranged  as.\n"
			"\t\tThe  value  of  R*C  must   match   the  total  number  of   processes.\n"
			"\t\tDefault: 1x1.\n"
			"\n"
			"\t-N <nbfs>\n"
			"\t\tSpecifies the number of BFSes performed on the input graph.\n"
			"\t\tDefault: 1.\n"
			"\n"
			"\t-r <root>\n"
			"\t\tSpecifies the vertex to be used as the root of the BFSes.\n"
			"\t\tDefault: a random root will be generated for each BFS.\n"
			"\n"
			"\t-z <root_seed>\n"
			"\t\tSpecifies the seed to be used to generate the sequence of root  vertices\n"
			"\t\tfor  the  BFSes.   This  option  has  no  effect  if  -r  is  specified.\n"
			"\t\tDefault: random value.\n"
			"\n"
			"\t-D\n"
			"\t\tSpecifies that the graph must be considered directed.\n"
			"\t\tDefault: undirected.\n"
			"\n"
			"\t-b [<m>-<n> | <thres>]\n"
			"\t\tSpecifies, for each BFS, the levels  in  which  the  communications  are\n"
			"\t\tperformed using a bitmap in place of a list of  vertices.   This  option\n"
			"\t\taccepts both an explicit range or a threshold.\n"
			"\t\tIn the first case the bitmap is  used  for  communications  in  all  the\n"
			"\t\tlevels between the specified extremes  (included).   A  range  of  '0-0'\n"
			"\t\tdisables the use of the bitmap.\n"
			"\t\tIf a threshold is specified, then the optimization is used for the three\n"
			"\t\tconsecutive bfs levels following the first step in which a percentage of\n"
			"\t\tnodes greater than 'thres' is visited.  The threshold  must  be  in  the\n"
			"\t\trange [0.0, 1.0].\n"
			"\t\tDefault: threshold, %lf.\n"
			"\n"
			"\t-u [<m>-<n> | <thres>]\n"
			"\t\tSpecifies,  for  each  BFS,  the   levels   in   which   the   bottom-up\n"
			"\t\texpansion is used in place of the top-down.  This option accepts both an\n"
			"\t\texplicit range or a threshold.\n"
			"\t\tIn the first case the bottom-up expansion is  used  in  all  the  levels\n"
			"\t\tbetween the specified extremes (included). A range of '0-0' disables the\n"
			"\t\tthe bottom-up strategy.  \n"
			"\t\tIf a threshold is specified, then the optimization is used  in each  BFS\n"
			"\t\tlevel where the ratio of frontier nodes to local nodes that haven't been\n"
			"\t\tvisited is greater than the threshold.  The threshold  must  be  in  the\n"
			"\t\trange [0.0, 1.0].\n"
			"\t\tDefault: threshold, %lf.\n"
			"\n"
			"\t-o\n"
			"\t\tEnables the  dump of level and predecessor  informations to files.  Each\n"
			"\t\tprocess writes two files: lvl_RANK and prd_RANK.  The BFS  for which the\n"
			"\t\tdata is dumped is always the last one (see -N option).\n"
			"\t\tDefault: disabled.\n"
			"\n"
			"\t-x\n"
			"\t\tEnables  the  exhaustive  search  for  the  best  bitmap  and  bottom-up\n"
			"\t\tthresholds  to  be  used with the input graph.  When using this  option,\n"
			"\t\tpossible  values  specified  with '-u'  and/or '-b' options are ignored.\n"
			"\t\tTHIS IS AN EXPERIMENTAL FEATURE.                                        \n"
			"\n"
			"RMAT input options:\n"
			"\t-S <scale>\n"
			"\t\tSpecifies the scale the of R-MAT graph (2^scale vertices).\n"
			"\t\tDefault value: 21.\n"
			"\n"
			"\t-E <edge_factor>\n"
			"\t\tSpecifies the average number of edges  per  vertex  (2^scale*edge_factor\n"
			"\t\ttotal edges).\n"
			"\t\tDefault value: 16.\n"
			"\n"
			"\t-d <outfile>\n"
			"\t\tDump the generated graph into a file called 'outfile'.\n"
			"\t\tDefault: graph not dumped.\n"
			"\n"
			"\t-s <graph_seed>\n"
			"\t\tSpecifies the seed to be used to generate the graph.\n"
			"\t\tDefault: 23.\n"
			"\n"
			"File input options:\n"
			"\t-f <graph_file>\n"
			"\t\tSpecifies the file containing the graph to visit.\n\n",
			bname, bname, BM_THRESHOLD, BU_THRESHOLD);
	}
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	exit(EXIT_SUCCESS);
}

uint64_t gcd(uint64_t a, uint64_t b) {
    uint64_t  t;
    while (b != 0){
        t = b;
        b = a % b;
        a = t;
    }
    return a;
}

uint64_t lcm(uint64_t a, uint64_t b) {
    return a*b / gcd(a, b);
}

int main(int argc, char *argv[]) {

	int s, t, gundir=1;
	int scale=21, edgef=16, nbfs=1;

	int64_t  i, j;
	uint64_t n, l, ned;
	uint64_t *edge=NULL;

	int	grmat=0;
	int	gread=0;

	LOCINT	*col=NULL;
	LOCINT	*row=NULL;
#ifdef BU_SEARCH
	LOCINT	*csr_col=NULL;
	LOCINT	*csr_row=NULL;
	LOCINT	*csr_gdg=NULL;
#endif
	LOCINT	*frt=NULL;
	LOCINT	*msk=NULL;
	int	*lvl=NULL;
	LOCINT	*deg=NULL;

	LOCINT	*vRbuf=NULL;
	LOCINT	*vRbuf2=NULL;
	int	*vRnum=NULL;

	LOCINT	*hSbuf=NULL;
	LOCINT	*hSbuf2=NULL;
	LOCINT	*hRbuf2=NULL;
	LOCINT	*hRbuf=NULL;
	int	*hSnum=NULL;
	int	*hRnum=NULL;
	int	*prd=NULL;

	double	bm_thr=BM_THRESHOLD;
	int	bm_lvl_min=-1;
	int	bm_lvl_max=-1;

	double	bu_thr=BU_THRESHOLD;
	int	bu_lvl_min=-1;
	int	bu_lvl_max=-1;

	MPI_Status  *status;
	MPI_Request *vrequest;
	MPI_Request *vrequest2;
	MPI_Request *hrequest;
	MPI_Request *hrequest2;

	uint64_t v0=0, gseed=23;
	
	int	 bfsoutw=0;
	int	 rootset=0;
	long int *rootseed=NULL;

	int dosearch=0;

	char *gfile=NULL, *p=NULL, c, *gdump=NULL;
	TIMER_DEF(0);

	lvldata_t lvlinfo[MAX_BFS_LEVEL];
	bfsdata_t *rundata=NULL;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	MPI_Comm_size(MPI_COMM_WORLD, &ntask);

	if (argc == 1) {
		usage(argv[0]);
	}

	while((c = getopt(argc, argv, ":p:d:f:n:r:S:E:N:s:Dhb:u:z:ox")) != EOF) {
                switch(c) {
                        case 'p':
				p = strtok(optarg, "x");
				if (!p) prexit("Invalid proc mesh field.\n");
				if (0 == sscanf(p, "%d", &R))
					prexit("Invalid number of rows for proc mesh (-p): %s\n", p);
				p = strtok(NULL, "x");
				if (!p) prexit("Invalid proc mesh field.\n");
				if (0 == sscanf(p, "%d", &C))
					prexit("Invalid number of columns for proc mesh (-p): %s\n", p);
                                break;
                        case 'd':
				grmat = 1;
				gdump = Strndup(optarg,MAX_LINE);
                                break;
                        case 'f':
				gread = 1;
				gfile = strdup(optarg);
                                break;
/* OBSOLETED
                        case 'n':
				gread = 1;
				if (0 == sscanf(optarg, "%"PRIu64, &N))
					prexit("Invalid number of vertices (-n): %s\n", optarg);
                                break;
*/  
			case 'r':
				if (0 == sscanf(optarg, "%"PRIu64, &v0))
					prexit("Invalid root vertex (-r): %s\n", optarg);
				rootset = 1;
                                break;
                        case 'S':
				grmat = 1;
				if (0 == sscanf(optarg, "%d", &scale))
					prexit("Invalid scale (-S): %s\n", optarg);
                                break;
                        case 'E':
				grmat = 1;
				if (0 == sscanf(optarg, "%d", &edgef))
					prexit("Invalid edge factor (-S): %s\n", optarg);
                                break;
                        case 'N':
				if (0 == sscanf(optarg, "%d", &nbfs))
					prexit("Invalid number of bfs (-N): %s\n", optarg);
                                break;
                        case 'D':
				gundir = 0;
                                break;
                        case 's':
				grmat = 1;
				if (0 == sscanf(optarg, "%"PRIu64, &gseed))
					prexit("Invalid seed for graph generator (-s): %s\n", optarg);
                                break;
                        case 'b':
				p = strchr(optarg, '-');
				if (!p) {
					if (0 == sscanf(optarg, "%lf", &bm_thr)) {
						prexit("Invalid bitmap threshold (-b): %s\n", p);
					}
				} else {
					p = strtok(optarg, "-");
					if (!p) prexit("Invalid bitmap level range field.\n");
					if (0 == sscanf(p, "%d", &bm_lvl_min))
						prexit("Invalid minimum bitmap level (-b): %s\n", p);
					p = strtok(NULL, "-");
					if (!p) prexit("Invalid bitmap level range field.\n");
					if (0 == sscanf(p, "%d", &bm_lvl_max))
						prexit("Invalid maximum bitmap level (-b): %s\n", p);
				}
				break;
                        case 'u':
				p = strchr(optarg, '-');
				if (!p) { // threshold parameter
					if (0 == sscanf(optarg, "%lf", &bu_thr)) {
						prexit("Invalid bottom-up threshold (-u): %s\n", p);
					}
				} else {
					p = strtok(optarg, "-");
					if (!p) prexit("Invalid bottom-up level range field.\n");
					if (0 == sscanf(p, "%d", &bu_lvl_min))
						prexit("Invalid minimum bottom-up level (-u): %s\n", p);
					p = strtok(NULL, "-");
					if (!p) prexit("Invalid bottom-up level range field.\n");
					if (0 == sscanf(p, "%d", &bu_lvl_max))
						prexit("Invalid maximum bottom-up level (-u): %s\n", p);
				}
                                break;
                        case 'z':
				rootseed = (long int *)Malloc(sizeof(rootseed));
				if (0 == sscanf(optarg, "%ld", rootseed))
					prexit("Invalid seed for root vertex sequence (-z): %s\n", optarg);
                                break;
                        case 'o':
				bfsoutw = 1;
                                break;
                        case 'x':
				dosearch = 1;
                                break;
                        case 'h':
                                usage(argv[0]);
                        case '?':
                                usage(argv[0]);
                        default:
                                usage(argv[0]);
		}
	}

	if (!gread && !grmat) grmat = 1;
	if (gread && grmat) {
		if (myid == 0) {
			fprintf(stdout, "RMAT and file input options cannot be mixed!\n");
		}
		usage(argv[0]);
	}
	if (gread) {
		//if (!gfile || !N) {
		if (!gfile) {
			if (myid == 0) {
				fprintf(stdout, "Graph file (-f) must be specified for file based bfs.\n");
			}
			usage(argv[0]);
		}
	}/* else {
		N = ((uint64_t)1)<<scale;
	}*/

	if (bm_lvl_min != -1 || bm_lvl_max != -1) {
		if (!((bm_lvl_min == 0 && bm_lvl_max == 0) ||
		      (bm_lvl_min > 0 && bm_lvl_max > bm_lvl_min))) {
			prexit("Invalid level range specified for bitmap transfers: %d-%d\n", bm_lvl_min, bm_lvl_max);
		}
	} else {
		if (bm_thr < 0.0 || bm_thr > 1.0) {
			prexit("Invalid threshold specified for bitmap transfers: %lf\n", bm_thr);
		}
	}

	if (bu_lvl_min != -1 || bu_lvl_max != -1) {
		if (!((bu_lvl_min == 0 && bu_lvl_max == 0) ||
		      (bu_lvl_min > 0 && bu_lvl_max >= bu_lvl_min))) {
			prexit("Invalid level range specified for bottom-up expand: %d-%d\n", bu_lvl_min, bu_lvl_max);
		}
		if (bu_lvl_min > 0 && !gundir)
			prexit("Bottom-up expand can only be used with UN-directed graphs!\n");
	} else {
		if (bu_thr < 0.0 || bu_thr > 100.0) {
			prexit("Invalid threshold specified for bottom-up expand: %lf\n", bu_thr);
		}
	}


	if (myid == 0) fprintf(stdout, "%s graph...", gread?"Reading":"Generating");
	TIMER_START(0);
	if (gread) {
		uint64_t maxv;
		ned = read_graph(myid, ntask, gfile, &edge, &maxv);
		MPI_Allreduce(MPI_IN_PLACE, &maxv, 1, MPI_UNSIGNED_LONG_LONG, MPI_MAX,  MPI_COMM_WORLD);
		// vertices starts from 0
	
		// make N a multiple of R, C and of 8*sizeof(vRbuf)
		N = maxv+1; // vertices starts from 0
		N = TO_MIN_MULTIPLE(N, lcm(R, lcm(C, sizeof(vRbuf)*8)));
	} else {
     		ned = gen_graph(scale, edgef, gseed, &edge);
		N = ((uint64_t)1)<<scale;
	}
	TIMER_STOP(0);
	if (myid == 0) fprintf(stdout, " done in %f secs\n", TIMER_ELAPSED(0));
	prstat(ned, gread?"Edges read from file":"Edges generated", 0);

	if (v0 >= N)
		prexit("Invalid start vertex: %"PRIu64".\n", v0);
	
	if (0 >= R || MAX_PROC_I < R || 0 >= C || MAX_PROC_J < C)
		prexit("R and C must be in range [1,%d] and [1,%d], respectively.\n",
		       MAX_PROC_I, MAX_PROC_J);

	if (0 != N%(R*C))
		prexit("N must be multiple of both R and C.\n");

	if (ntask != R*C)
		prexit("The number of MPI tasks (%d) must be equal to R*C (%d).\n", ntask, R*C);
#ifndef _LARGE_LVERTS_NUM
	if ((N/R) > UINT32_MAX) {
		prexit("Number of vertics per processor too big (%"LOCPRI"), please"
		       "define _LARGE_LVERTS_NUM macro in %s.\n", (N/R), __FILE__);
	}
#endif

	myrow = myid/C;
	mycol = myid%C;

	row_bl = N/(R*C); /* adjacency matrix rows per block:	 N/(RC) */
	col_bl = N/C;     /* adjacency matrix columns per block: N/C */
	row_pp = N/R;     /* adjacency matrix rows per proc:	 N/(RC)*C = N/R */
	
	bmaplen = (row_bl+BITS(vRbuf)-1)/BITS(vRbuf);

	if (myid == 0) {
		fprintf(stdout, "Total number of vertices (N): %"PRIu64"\n", N);
		fprintf(stdout, "Processor mesh rows (R): %d\n", R);
		fprintf(stdout, "Processor mesh columns (C): %d\n", C);
		fprintf(stdout, "Number of rows per block (N/(R*C)): %"LOCPRI"\n", row_bl);
		fprintf(stdout, "Number of columns per block (N/C): %"LOCPRI"\n", col_bl);
		fprintf(stdout, "Total rows per processor (N/R): %"LOCPRI"\n", row_pp);
		fprintf(stdout, "Size of transfer bitmap: %zu\n", bmaplen*sizeof(*vRbuf));
		fprintf(stdout, "Size of local integer: %zu\n", sizeof(LOCINT));
		if (gread) {
			fprintf(stdout, "Reading graph from file: %s\n", gfile);
		} else {
			fprintf(stdout, "RMAT graph scale: %d\n", scale);
			fprintf(stdout, "RMAT graph edge factor: %d\n", edgef);
			fprintf(stdout, "RMAT generator seed: %"PRIu64"\n", gseed);
			fprintf(stdout, "Number of breadth first searches: %d\n", nbfs);
		}
		fprintf(stdout, "Graph visited as: %sdirected\n", gundir?"un-":"");
		if (bm_lvl_min != -1) {
			fprintf(stdout, "Bfs level range for bitamp transfers: %d-%d\n", bm_lvl_min, bm_lvl_max);
		} else {
			fprintf(stdout, "Bfs threshold for bitamp transfers: %lf\n", bm_thr);
		}
		if (bu_lvl_min != -1) {
			fprintf(stdout, "Bottom-up level range for expand: %d-%d\n\n", bu_lvl_min, bu_lvl_max);
		} else {
			fprintf(stdout, "Bottom-up threshold for expand: %lf\n\n", bu_thr);
		}
	}
	MPI_Barrier(MPI_COMM_WORLD);

	/* fill processor mesh */
	memset(pmesh, -1, sizeof(pmesh));
	for(i = 0; i < R; i++)
		for(j = 0; j < C; j++)
			pmesh[i][j] = i*C + j;

	if (gdump) dump_graph(gdump, ned, edge);

	if (gundir) {
		if (myid == 0) fprintf(stdout, "Mirroring graph...");
		TIMER_START(0);
		edge = mirror(edge, &ned);
		TIMER_STOP(0);
		if (myid == 0) fprintf(stdout, " done in %f secs\n", TIMER_ELAPSED(0));
	}
	if (ntask > 1) {
		if (myid == 0) fprintf(stdout, "Partitioning graph...");
		TIMER_START(0);
		ned = part_graph(myid, ntask, &edge, ned);
		TIMER_STOP(0);
		if (myid == 0) fprintf(stdout, " done in %f secs\n", TIMER_ELAPSED(0));
		prstat(ned, "Edges assigned after partitioning", 0);
	}
#ifndef _LARGE_LVERTS_NUM
	if (ned > UINT32_MAX) {
		fprintf(stderr,
			"[%d] Too many vertices assigned to me, %"PRIu64" (max=%u)\n",
			myid, ned, UINT32_MAX);
		exit(EXIT_FAILURE);
	}
#endif
	if (ntask > 1) {
		if (myid == 0) fprintf(stdout, "Verifying partitioning...");
		TIMER_START(0);
		for(n = 0; n < ned; n++) {
			if (EDGE2PROC(edge[2*n], edge[2*n+1]) != myid) {
				fprintf(stdout,
					"[%d] error, received edge (%"PRIu64", %"PRIu64"), should have been sent to %d\n",
					myid, edge[2*n],edge[2*n+1], EDGE2PROC(edge[2*n], edge[2*n+1]));
				break;
			}
		}
		s = (n != ned);
		MPI_Allreduce(&s, &t, 1, MPI_INT, MPI_LOR, MPI_COMM_WORLD);
		TIMER_STOP(0);
		if (t)	prexit("Error in 2D decomposition.\n");
		else	if (myid == 0) fprintf(stdout, " done in %f secs\n", TIMER_ELAPSED(0));
	}
	initcuda();
	
	if (myid == 0) fprintf(stdout, "Removing multi-edges...");
	deg = (LOCINT *)Malloc(row_pp*sizeof(*deg));

	TIMER_START(0);
	l = norm_graph(edge, ned, deg);
	TIMER_STOP(0);
	if (myid == 0) fprintf(stdout, " done in %f secs\n", TIMER_ELAPSED(0));
	//prstat(ned-l, "Multi-edges removed", 0);
	prstat(l, "Final number of edges per proc", 0);
	ned = l;

#ifndef _LARGE_LVERTS_NUM
	if (!verify_32bit_fit(edge, ned))
		MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
#endif

	if (myid == 0) fprintf(stdout, "Creating CSC...");
	TIMER_START(0);
	build_csc(edge, ned, &col, &row);
	TIMER_STOP(0);
	if (myid == 0) fprintf(stdout, " done in %f secs\n", TIMER_ELAPSED(0));
#ifdef BU_SEARCH
	if (myid == 0) fprintf(stdout, "Creating CSR...");
	TIMER_START(0);
	build_csr(edge, ned, &csr_col, &csr_row, &csr_gdg);
	TIMER_STOP(0);
	if (myid == 0) fprintf(stdout, " done in %f secs\n", TIMER_ELAPSED(0));
#endif

	n = initcuda_data(ned);

	free(edge);
	prstat(n>>20, "Device memory allocated (MB)", 0);

	frt = (LOCINT *)Malloc(row_bl*sizeof(*frt));
	lvl = (int *)Malloc(row_pp*sizeof(*lvl));
	msk = (LOCINT *)Malloc(((row_pp+BITS(msk)-1)/BITS(msk))*sizeof(*msk));
	prd = (int *)CudaMallocHostSet(row_pp*sizeof(*prd), 0);
		
	vRbuf = (LOCINT *)CudaMallocHostSet(col_bl*sizeof(*vRbuf), 0);
	vRbuf2 = (LOCINT *)CudaMallocHostSet(col_bl*sizeof(*vRbuf2), 0);
	vRnum = (int *)CudaMallocHostSet(R*sizeof(*vRbuf), 0);
	hSbuf = (LOCINT *)CudaMallocHostSet(row_pp*sizeof(*hSbuf), 0);
	hSbuf2 = (LOCINT *)CudaMallocHostSet(row_pp*sizeof(*hSbuf2), 0);
	hSnum = (int *)CudaMallocHostSet(C*sizeof(*hSnum), 0);
	hRbuf = (LOCINT *)CudaMallocHostSet(row_pp*sizeof(*hRbuf), 0);
	hRbuf2 = (LOCINT *)CudaMallocHostSet(row_pp*sizeof(*hRbuf), 0);
	hRnum = (int *)CudaMallocHostSet(C*sizeof(*hRnum), 0);

	rundata = (bfsdata_t *)Malloc(nbfs*sizeof(*rundata));

	MPI_Comm_split(MPI_COMM_WORLD, myrow, mycol, &Row_comm);
	MPI_Comm_split(MPI_COMM_WORLD, mycol, myrow, &Col_comm);
#ifdef BU_SEARCH
	// Is this really useful?! Investigate...
	MPI_Allreduce(MPI_IN_PLACE, csr_gdg, row_pp, LOCINT_MPI, MPI_SUM, Row_comm);
#endif
	status  =  (MPI_Status *)Malloc(MAX(C,R)*sizeof(*status));
	vrequest = (MPI_Request *)Malloc(MAX(C,R)*sizeof(*vrequest));
	vrequest2 = (MPI_Request *)Malloc(MAX(C,R)*sizeof(*vrequest2));
	hrequest = (MPI_Request *)Malloc(MAX(C,R)*sizeof(*hrequest));
	hrequest2 = (MPI_Request *)Malloc(MAX(C,R)*sizeof(*hrequest2));

	//char gfname[256];
	//snprintf(gfname, 256, "mpi_send_log_P%d_%dx%d.txt", myid, myrow, mycol);
	//gfp = Fopen(gfname, "w");

	// exchange for mpi warm-up
	exchange_vert4(frt, row_bl, vRbuf, row_bl, vRnum, vrequest, status, 1);
	exchange_vert4_bmap(vRbuf2, bmaplen, vrequest2, status, 1);

	for(i = 0; i < C; i++) hSnum[i] = row_bl;
	exchange_horiz4(hSbuf, row_bl, hSnum, hRbuf, row_bl, hRnum, hrequest, status, 1);
	exchange_horiz4_bmap(hSbuf2, hRbuf2, bmaplen, hrequest2, status, 1);

	if (!dosearch) {
		//fprintf(gfp, "MARK\n");
		if (!rootset) set_root_seed(rootseed);

		for(i = 0; i < nbfs; i++) {
#ifndef BU_SEARCH
			resetcuda(ned, col, row);
#else
			resetcuda_data(ned, col, row, csr_col, csr_row, csr_gdg);
#endif
			if (!rootset) v0 = get_root(col);
			if (myid == 0) {
				fprintf(stdout,
					"\n\nBFS %"PRIu64" starts from processor %d, vertex %"PRIu64".\n\n",
					i, VERT2PROC(v0), v0);
			}
			int nlvl = bfs(gundir, row, col, frt, msk, lvl, prd, deg, v0,
				       vRbuf, vRbuf2, vRnum, hSbuf, hSbuf2, hSnum, hRbuf, hRbuf2,
				       hRnum, vrequest, vrequest2, hrequest, hrequest2,
				       bm_thr, bm_lvl_min, bm_lvl_max,
				       bu_thr, bu_lvl_min, bu_lvl_max,
				       status, rundata+i, lvlinfo,
				       bfsoutw ? i==(nbfs-1) : 0);
			
			if (myid == 0) {
				for(j = 0; j < nlvl; j++) {
					fprintf(stdout, 
						"BFS round %3"PRIu64"... [%7.3lf%%] %s %s %"PRIu64"\n",
						j+1,
						(lvlinfo[j].nedges*100.0)/N,
						lvlinfo[j].scanbu?"BU":"TD",
						(lvlinfo[j].inmsk||lvlinfo[j].outmsk)?"BT":"LT",
						lvlinfo[j].nedges);
				}
				fprintf(stdout, "\nElapsed time: %f secs\n", rundata[i].totalt);
				fprintf(stdout, "Traversed edges (%sdirected): %"PRIu64"\n", gundir?"un-":"", rundata[i].nedges);
				fprintf(stdout, "Measured TEPS: %lf\n", ((double)rundata[i].nedges)/rundata[i].totalt);
			}
		//fprintf(gfp, "MARK\n");
		}
	} else {
		double best_uthr=0.1;
		double best_bthr=1.0E-5;
		double best_teps = 0;

		for(s = 0; s < 20; s++) {
			for(t = 0; t < 20; t++) {

				bu_thr = BU_TH_SEARCH_MIN + s*(BU_TH_SEARCH_MAX-BU_TH_SEARCH_MIN)/20;
				bm_thr = BM_TH_SEARCH_MIN + t*(BM_TH_SEARCH_MAX-BM_TH_SEARCH_MIN)/20;
			
				if (!rootset) {
					long int seed = set_root_seed(rootseed);
					if (!rootseed) {
						rootseed = (long int *)Malloc(sizeof(rootseed));
					}
					// if rootseed is not specified then we need to
					// save the first one, randomly generated, to 
					// perform the runs on the same root sequence
					*rootseed = seed;
				}

				double teps = 0;
				for(i = 0; i < nbfs; i++) {
#ifndef BU_SEARCH
					resetcuda(ned, col, row);
#else
					resetcuda_data(ned, col, row, csr_col, csr_row, csr_gdg);
#endif
					if (!rootset) v0 = get_root(col);
					
					//fprintf(stderr, "testing with bm_thr=%lf bu_thr=%lf\n", bm_thr, bu_thr);
					bfs(gundir, row, col, frt, msk, lvl, prd, deg, v0,
					    vRbuf, vRbuf2, vRnum, hSbuf, hSbuf2, hSnum, hRbuf, hRbuf2,
					    hRnum, vrequest, vrequest2, hrequest, hrequest2,
					    bm_thr, bm_lvl_min, bm_lvl_max,
					    bu_thr, bu_lvl_min, bu_lvl_max,
					    status, rundata+i, lvlinfo,
					    bfsoutw ? i==(nbfs-1) : 0);
					
					if (myid == 0) {
						teps += ((double)rundata[i].nedges)/rundata[i].totalt;
					}
				}
				if (teps/nbfs > best_teps) {
					best_uthr = bu_thr;
					best_bthr = bm_thr;
					best_teps = teps/nbfs;
				}
			}
		}
		if (myid == 0) {
			fprintf(stdout, "\nBest thresholds found:\n");
			fprintf(stdout, "\t-u %lf\n", best_uthr);
			fprintf(stdout, "\t-b %lf\n", best_bthr);
			fprintf(stdout, "\tTEPS: %lf\n",  best_teps);
		}
	}

	cancel_vert(vrequest);
	cancel_vert(vrequest2);
	cancel_horiz(hrequest);
	cancel_horiz(hrequest2);

	if (!dosearch) {
#ifdef _TIMINGS
		for(i = 0; i < nbfs; i++) {
			if (myid == 0) fprintf(stdout, "\nTimings for BFS %"PRId64":\n", i);
			prstat(rundata[i].vtrans*1.0E+6, "  exchange_vert (us)", 0);
			prstat(rundata[i].expand*1.0E+6, "  scan_col_csc (us)", 0);
			prstat(rundata[i].htrans*1.0E+6, "  exchange_horiz (us)", 0);
			prstat(rundata[i].update*1.0E+6, "  append_rows (us)", 0);
			prstat(rundata[i].allred*1.0E+6, "  all_gather (us)", 0);
			prstat((rundata[i].expand+rundata[i].update)*1.0E+6, "  total computation time (us)", 0);
			prstat((rundata[i].vtrans+rundata[i].htrans+rundata[i].allred)*1.0E+6, "  total exchange time (us)", 0);
		}
#endif
		if (myid == 0) {
			print_statsG500(rundata, nbfs);
		}
	}

	//fclose(gfp);
	free(col);
	free(row);
#ifdef BU_SEARCH
	free(csr_row);
	free(csr_col);
	free(csr_gdg);
#endif	
	free(lvl);
	free(msk);
	free(frt);
	free(deg);
	free(status);
	free(vrequest);
	free(hrequest);
	free(vrequest2);
	free(hrequest2);
	if (gfile) free(gfile);
	if (gdump) free(gdump);
	if (rootseed) free(rootseed);
	free(rundata);

	fincuda();
	CudaFreeHost(prd);
	CudaFreeHost(vRbuf);
	CudaFreeHost(vRbuf2);
	CudaFreeHost(vRnum);
	CudaFreeHost(hSbuf);
	CudaFreeHost(hSbuf2);
	CudaFreeHost(hSnum);
	CudaFreeHost(hRbuf);
	CudaFreeHost(hRbuf2);
	CudaFreeHost(hRnum);

	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Comm_free(&Row_comm);
	MPI_Comm_free(&Col_comm);
	MPI_Finalize();
	exit(EXIT_SUCCESS);
}
