/* 
 * Copyright 2013-2016 Mauro Bisson <mauro.bis@gmail.com>
 * 		       Massimo Bernaschi <massimo.bernaschi@gmail.com>
 * 		       Enrico Mastrostefano <babil.babilon@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <sys/time.h>
#include <cuda.h>
#include <mpi.h>

//#define USE_THRUST
#ifdef USE_THRUST
#include <thrust/device_vector.h>
#include <thrust/scan.h>
#else
#include <cub/device/device_scan.cuh>
#include <cub/device/device_reduce.cuh>
#include <cub/device/device_radix_sort.cuh>
using namespace cub;
#endif

#include "cudamacro.h"
#include "bfs2d.h"

#define THREADS (128)
// Best values for SINGLE GPU runs:
//	K20x: 6
//	K80:  12
#ifndef ROWXTH
#define ROWXTH 6
#endif

__device__ __constant__ uint64_t dN;
__device__ __constant__ LOCINT drow_bl;
__device__ __constant__ LOCINT dcol_bl;
__device__ __constant__ LOCINT drow_pp;

__device__ __constant__ int dC;
__device__ __constant__ int dR;
__device__ __constant__ int dmyrow;
__device__ __constant__ int dmycol;

static LOCINT	msk_len=0;
static LOCINT	*d_msk=NULL;
static int	*d_lvl=NULL;
static int	*d_prd=NULL;

static LOCINT	*d_col=NULL;
static LOCINT	*d_row=NULL;
static LOCINT	*d_deg=NULL;

#ifdef BU_SEARCH
static LOCINT	*d_csr_col=NULL;
static LOCINT	*d_csr_row=NULL;
static LOCINT	*d_csr_deg=NULL;
static LOCINT	*d_cmsk=NULL;
static LOCINT	*d_zdmsk=NULL;
#endif

static LOCINT	*d_rbuf=NULL;
static LOCINT	*d_cbuf=NULL;

static LOCINT	*d_fbmap=NULL;

static LOCINT	*d_sbuf=NULL;
static int	*d_snum=NULL;

#ifndef USE_THRUST
static LOCINT	*d_cubtmp=NULL;
static size_t	cubtmp_sz=0;
#endif
 
cudaEvent_t     start, stop;
cudaStream_t    stream[2];

// returns the index of the maximum i | v[i] <= val
__device__ LOCINT bmaxlt(const LOCINT *__restrict__ v, LOCINT num, LOCINT val) {

	LOCINT	min = 0;
	LOCINT	max = num-1;
	LOCINT	mid = max >> 1;

	while(min <= max) {

		if (v[mid] == val)	return mid;
		if (v[mid]  < val)	min = mid+1;
		else			max = mid-1;
		mid = (max>>1)+(min>>1)+((min&max)&1);
	}
	return mid;
} 
	
__global__ void read_edge_count(const LOCINT *__restrict__ deg,
				const LOCINT *__restrict__ rbuf,
				LOCINT n, LOCINT *cbuf) {
	
	const uint32_t tid = blockDim.x*blockIdx.x + threadIdx.x;

	if (tid >= n) return;
	cbuf[tid] = deg[rbuf[tid]];
	return;
}

#ifdef USE_LDG
#define LDG(x) (__ldg(&(x)))
#else
#define LDG(x) (x)
#endif

__global__ void scan_col(const LOCINT *__restrict__ row,  const LOCINT *__restrict__ col, LOCINT nrow, 
			 const LOCINT *__restrict__ rbuf, const LOCINT *__restrict__ cbuf, LOCINT ncol,
			 LOCINT *msk, int *prd, int *lvl, int level,
			 LOCINT *sbuf, int *snum) {
	
	LOCINT r[ROWXTH], c[ROWXTH];
	LOCINT m[ROWXTH], q[ROWXTH], i[ROWXTH];
	const uint32_t tid = (blockDim.x*blockIdx.x + threadIdx.x)*ROWXTH;

	if (tid >= nrow) return;

	i[0] = bmaxlt(cbuf, ncol, tid);
	for(; (i[0]+1 < ncol) && (tid+0) >= LDG(cbuf[i[0]+1]); i[0]++);
	#pragma unroll
	for(int k = 1; k < ROWXTH; k++)
		for(i[k]=i[k-1]; (i[k]+1 < ncol) && (tid+k) >= LDG(cbuf[i[k]+1]); i[k]++);

	#pragma unroll
	for(int k = 0; k < ROWXTH; k++) c[k] = LDG(rbuf[i[k]]);

	#pragma unroll
	for(int k = 0; k < ROWXTH; k++)
		if (tid+k < nrow)
			r[k] = LDG(row[LDG(col[c[k]])+(tid+k)-LDG(cbuf[i[k]])]);

	#pragma unroll
	for(int k = 0; k < ROWXTH; k++) m[k] = ((LOCINT)1) << (r[k]%BITS(msk));

	#pragma unroll
	for(int k = 0; k < ROWXTH; k++) {

		if (tid+k >= nrow) break;
		if (LDG(msk[r[k]/BITS(msk)])&m[k])	//continue;
			q[k] = m[k];			// the if below will eval to false...
		else
			q[k] = atomicOr(msk+r[k]/BITS(msk), m[k]);

		if (!(m[k]&q[k])) {
			int hproc = r[k] / drow_bl;
			int off = atomicAdd(snum+hproc, 1);
			sbuf[hproc*drow_bl + off] = (hproc != dmycol) ? r[k] : CUDA_MYLOCI2LOCJ(r[k]);
			if (hproc == dmycol) lvl[r[k]] = level;
			prd[r[k]] = c[k];
		}
	}
	return;
}

__global__ void 
//__launch_bounds__(128, 16)
scan_col_bmap(const LOCINT *__restrict__ row,  const LOCINT *__restrict__ col, LOCINT nrow, 
			      const LOCINT *__restrict__ rbuf, const LOCINT *__restrict__ cbuf, LOCINT ncol,
			      LOCINT *msk, int *prd, int *lvl, int level,
			      LOCINT *sbuf) {

	LOCINT r[ROWXTH], c[ROWXTH];
	LOCINT m[ROWXTH], q[ROWXTH], i[ROWXTH];
	const uint32_t tid = (blockDim.x*blockIdx.x + threadIdx.x)*ROWXTH;

	if (tid >= nrow) return;

	i[0] = bmaxlt(cbuf, ncol, tid);
	for(; (i[0]+1 < ncol) && (tid+0) >= LDG(cbuf[i[0]+1]); i[0]++);

	#pragma unroll
	for(int k = 1; k < ROWXTH; k++)
		for(i[k]=i[k-1]; (i[k]+1 < ncol) && (tid+k) >= LDG(cbuf[i[k]+1]); i[k]++);

	#pragma unroll
	for(int k = 0; k < ROWXTH; k++) c[k] = LDG(rbuf[i[k]]);
#if 1
	#pragma unroll
	for(int k = 0; k < ROWXTH; k++)
		r[k] = (tid+k < nrow) ? LDG(row[LDG(col[c[k]])+(tid+k)-LDG(cbuf[i[k]])]) : 1;

	#pragma unroll
	for(int k = 0; k < ROWXTH; k++)
		m[k] = ((LOCINT)1) << (r[k]%BITS(msk));
#if 0
	// this branch aggregates atomicOr()s on the same bmap element
	LOCINT bo[ROWXTH];

	#pragma unroll
	for(int k = 0; k < ROWXTH; k++)
		bo[k] = r[k]/BITS(msk);

	// reuse i...
	i[0] = (tid+0 < nrow) ? m[0]: 0;
	#pragma unroll
	for(int k = 1; k < ROWXTH; k++) {
		if (tid+k < nrow) {
			if (bo[k] == bo[k-1])	i[k] = m[k] | i[k-1];
			else			i[k] = m[k];
		} else {
			i[k] = 0;
		}
	}

	q[ROWXTH-1] = i[ROWXTH-1] ? atomicOr(msk+bo[ROWXTH-1], i[ROWXTH-1]) : m[ROWXTH-1];

	#pragma unroll
        for(int k = ROWXTH-2; k >= 0; k--) {
		if (i[k]) {
			q[k] = (bo[k] != bo[k+1] || !i[k+1]) ? atomicOr(msk+bo[k], i[k]) : q[k+1];
		} else q[k] = m[k];
	}
#else
	// reuse i...
	#pragma unroll
	for(int k = 0; k < ROWXTH; k++)
		i[k] = (tid+k < nrow) ? LDG(msk[r[k]/BITS(msk)])&m[k] : 1;

	#pragma unroll
        for(int k = 0; k < ROWXTH; k++)
		q[k] = (i[k]) ? m[k] : atomicOr(msk+r[k]/BITS(msk), m[k]);
#endif
	#pragma unroll
        for(int k = 0; k < ROWXTH; k++)
		i[k] = !(m[k]&q[k]);

	#pragma unroll
        for(int k = 0; k < ROWXTH; k++)
		if (i[k]) atomicOr(sbuf + r[k]/BITS(msk), m[k]);

	#pragma unroll
        for(int k = 0; k < ROWXTH; k++) {
		if (i[k]) {
			if (r[k]/drow_bl == dmycol) lvl[r[k]] = level;
			prd[r[k]] = c[k];
		}
	}
#else
	#pragma unroll
	for(int k = 0; k < ROWXTH; k++)
		if (tid+k < nrow)
			r[k] = LDG(row[LDG(col[c[k]])+(tid+k)-LDG(cbuf[i[k]])]);

	#pragma unroll
	for(int k = 0; k < ROWXTH; k++) m[k] = ((LOCINT)1) << (r[k]%BITS(msk));

	#pragma unroll
	for(int k = 0; k < ROWXTH; k++) {

		if (tid+k >= nrow) break;
		if (LDG(msk[r[k]/BITS(msk)])&m[k])	//continue;
			q[k] = m[k];			// the if below will eval to false...
		else
			q[k] = atomicOr(msk+r[k]/BITS(msk), m[k]);

		if (!(m[k]&q[k])) {
			int hproc = r[k] / drow_bl;

			atomicOr(sbuf + r[k]/BITS(msk), m[k]);

			if (hproc == dmycol) lvl[r[k]] = level;
			prd[r[k]] = c[k];
		}
	}
#endif
	return;
}


#ifdef BU_SEARCH
__global__ void scan_col_bu(const LOCINT *__restrict__ row,  const LOCINT *__restrict__ col, LOCINT ncol,
			    const LOCINT *__restrict__ rbuf, const LOCINT *__restrict__ cbuf, LOCINT nrow,
			    //const unsigned char *__restrict__ cmsk, LOCINT *msk, int *prd, int *lvl, int level,
			    const LOCINT *__restrict__ cmsk, LOCINT *msk, int *prd, int *lvl, int level,
			    LOCINT *sbuf, int *snum) {
	
	LOCINT r[ROWXTH], c[ROWXTH];
	LOCINT m[ROWXTH], q[ROWXTH], i[ROWXTH];
	const uint32_t tid = (blockDim.x*blockIdx.x + threadIdx.x)*ROWXTH;

	if (tid >= ncol) return;

	i[0] = bmaxlt(cbuf, nrow, tid);
	for(; (i[0]+1 < nrow) && (tid+0) >= LDG(cbuf[i[0]+1]); i[0]++);

	#pragma unroll
	for(int k = 1; k < ROWXTH; k++)
		for(i[k]=i[k-1]; (i[k]+1 < nrow) && (tid+k) >= LDG(cbuf[i[k]+1]); i[k]++);

	#pragma unroll
	for(int k = 0; k < ROWXTH; k++) r[k] = LDG(rbuf[i[k]]);
	//for(int k = 0; k < ROWXTH; k++) c[k] = LDG(rbuf[i[k]]);

	#pragma unroll
	for(int k = 0; k < ROWXTH; k++)
		if (tid+k < ncol)
			c[k] = LDG(col[LDG(row[r[k]])+(tid+k)-LDG(cbuf[i[k]])]);

	#pragma unroll
	for(int k = 0; k < ROWXTH; k++) m[k] = ((LOCINT)1) << (r[k]%BITS(msk));
	
	#pragma unroll
	for(int k = 0; k < ROWXTH; k++) {

		if (tid+k >= ncol) break;
		if (LDG(cmsk[c[k]/BITS(cmsk)]) & ( ((LOCINT)1) << (c[k]%BITS(cmsk)))) {
			
			if (LDG(msk[r[k]/BITS(msk)])&m[k]) {	//continue;
				q[k] = m[k];			// the if below will eval to false...
			} else {
				q[k] = atomicOr(msk+r[k]/BITS(msk), m[k]);
			}
			if (!(m[k]&q[k])) {
				int hproc = r[k] / drow_bl;
				int off = atomicAdd(snum+hproc, 1);
				sbuf[hproc*drow_bl + off] = (hproc != dmycol) ? r[k] : CUDA_MYLOCI2LOCJ(r[k]);
				if (hproc == dmycol) lvl[r[k]] = level;
				prd[r[k]] = c[k];
			}
			//break;
			//for(; k < ROWXTH-1 && r[k] == r[k+1]; k++);
		}
	}
	return;
}

__global__ void scan_col_bmap_bu(const LOCINT *__restrict__ row,  const LOCINT *__restrict__ col, LOCINT ncol,
				 const LOCINT *__restrict__ rbuf, const LOCINT *__restrict__ cbuf, LOCINT nrow,
				 //const unsigned char *__restrict__ cmsk, LOCINT *msk, int *prd, int *lvl, int level,
				 const LOCINT *__restrict__ cmsk, LOCINT *msk, int *prd, int *lvl, int level,
				 LOCINT *sbuf) {
	
	LOCINT r[ROWXTH], c[ROWXTH];
	LOCINT m[ROWXTH], q[ROWXTH], i[ROWXTH];
	const uint32_t tid = (blockDim.x*blockIdx.x + threadIdx.x)*ROWXTH;

	if (tid >= ncol) return;

	i[0] = bmaxlt(cbuf, nrow, tid);
	for(; (i[0]+1 < nrow) && (tid+0) >= LDG(cbuf[i[0]+1]); i[0]++);

	#pragma unroll
	for(int k = 1; k < ROWXTH; k++)
		for(i[k]=i[k-1]; (i[k]+1 < nrow) && (tid+k) >= LDG(cbuf[i[k]+1]); i[k]++);

	#pragma unroll
	for(int k = 0; k < ROWXTH; k++) r[k] = LDG(rbuf[i[k]]);
	//for(int k = 0; k < ROWXTH; k++) c[k] = LDG(rbuf[i[k]]);
#if 0
	#pragma unroll
	for(int k = 0; k < ROWXTH; k++)
		c[k] = (tid+k < ncol) ? LDG(col[LDG(row[r[k]])+(tid+k)-LDG(cbuf[i[k]])]) : 1;
		//r[k] = (tid+k < nrow) ? LDG(row[LDG(col[c[k]])+(tid+k)-LDG(cbuf[i[k]])]) : 1;
	
	#pragma unroll
	for(int k = 0; k < ROWXTH; k++)
		m[k] = ((LOCINT)1) << (r[k]%BITS(msk));

	// reuse i...	
	#pragma unroll
	for(int k = 0; k < ROWXTH; k++)
		i[k] = (tid+k < ncol) ? LDG(cmsk[c[k]/BITS(cmsk)]) & ( ((LOCINT)1) << (c[k]%BITS(cmsk))) : 0;
	
	#pragma unroll
	for(int k = 0; k < ROWXTH; k++)
		i[k] = (tid+k < ncol && i[k]) ? LDG(msk[r[k]/BITS(msk)])&m[k] : 1;

	#pragma unroll
        for(int k = 0; k < ROWXTH; k++)
		q[k] = (i[k]) ? m[k] : atomicOr(msk+r[k]/BITS(msk), m[k]);

	#pragma unroll
        for(int k = 0; k < ROWXTH; k++)
		i[k] = !(m[k]&q[k]);

	#pragma unroll
        for(int k = 0; k < ROWXTH; k++)
		if (i[k]) atomicOr(sbuf + r[k]/BITS(msk), m[k]);

	#pragma unroll
        for(int k = 0; k < ROWXTH; k++) {
		if (i[k]) {
			if (r[k]/drow_bl == dmycol) lvl[r[k]] = level;
			prd[r[k]] = c[k];
		}
	}
#else
	#pragma unroll
	for(int k = 0; k < ROWXTH; k++)
		if (tid+k < ncol)
			c[k] = LDG(col[LDG(row[r[k]])+(tid+k)-LDG(cbuf[i[k]])]);
			//r[k] = LDG(row[LDG(col[c[k]])+(tid+k)-LDG(cbuf[i[k]])]);

	#pragma unroll
	for(int k = 0; k < ROWXTH; k++) m[k] = ((LOCINT)1) << (r[k]%BITS(msk));
	
	#pragma unroll
	for(int k = 0; k < ROWXTH; k++) {

		if (tid+k >= ncol) break;

		if (LDG(cmsk[c[k]/BITS(cmsk)]) & ( ((LOCINT)1) << (c[k]%BITS(cmsk)))) {

			if (LDG(msk[r[k]/BITS(msk)])&m[k])	//continue;
				q[k] = m[k];			// the if below will eval to false...
			else
				q[k] = atomicOr(msk+r[k]/BITS(msk), m[k]);
			
			if (!(m[k]&q[k])) {
				int hproc = r[k] / drow_bl;

				atomicOr(sbuf + r[k]/BITS(msk), m[k]);

				if (hproc == dmycol) lvl[r[k]] = level;
				prd[r[k]] = c[k];
			}
		}
	}
#endif
	return;
}
#endif

__global__ void append_row(const LOCINT *__restrict__ row,  LOCINT n,
			   const LOCINT *__restrict__ cbuf, LOCINT np,
			   LOCINT *msk,  int *prd, int *lvl,
			   int level, LOCINT *frt, int *addfrt) {

	LOCINT	 r, m, q;
	const uint32_t tid = blockDim.x*blockIdx.x + threadIdx.x;

	if (tid >= n) return;

	r = row[tid];
	m = ((LOCINT)1) << (r%BITS(msk));
	if (msk[r/BITS(msk)]&m) return;

	q = atomicOr(msk+r/BITS(msk), m);

	if (!(m&q)) {
		int off = atomicAdd(addfrt, 1);
		frt[off] = CUDA_MYLOCI2LOCJ(r);
		lvl[r] = level;
		
		q = bmaxlt(cbuf, np, tid);
		for(; (q+1 < np) && tid >= cbuf[q+1]; q++);
		prd[r] = -(1+q);
	}
	return;
}

__global__ void append_row_bmap(const LOCINT *__restrict__ row,  LOCINT n,
				LOCINT *msk,  int *prd, int *lvl,
				int level, LOCINT *frt) {

	LOCINT	 r, m, q;
	const uint32_t tid = blockDim.x*blockIdx.x + threadIdx.x;

	if (tid >= n) return;

	r = row[tid];
	m = ((LOCINT)1) << (r%BITS(msk));
	if (msk[r/BITS(msk)]&m) return;

	q = atomicOr(msk+r/BITS(msk), m);

	if (!(m&q)) {
		lvl[r] = level;

		r -= dmycol*drow_bl;
		atomicOr(frt+r/BITS(msk), ((LOCINT)1) << (r%BITS(msk)) );
	}
	return;
}

static size_t tot_dev_mem = 0;
static void *CudaMallocSet(size_t size, int val) {

        void *ptr;

        MY_CUDA_CHECK( cudaMalloc(&ptr, size) );
        MY_CUDA_CHECK( cudaMemset(ptr, val, size) );
        tot_dev_mem += size;

        return ptr;
}

void *CudaMallocHostSet(size_t size, int val) {

        void *ptr;

        MY_CUDA_CHECK( cudaMallocHost(&ptr, size) );
        memset(ptr, val, size);
        return ptr;
}

void CudaFreeHost(void *ptr) {

        MY_CUDA_CHECK( cudaFreeHost(ptr) );
        return;
}

__global__ void set_degree(LOCINT *col, LOCINT *deg, LOCINT n) {

	const uint32_t tid = blockDim.x*blockIdx.x + threadIdx.x;

	if (tid >= n) return;
	deg[tid] = col[tid+1] - col[tid];
	return;

}

void set_mlp_cuda(LOCINT row, int level, int pred) {

	LOCINT v;
	MY_CUDA_CHECK( cudaMemcpy(&v, d_msk+row/BITS(d_msk), sizeof(v), cudaMemcpyDeviceToHost) );
	v |= (1ULL<<(row%BITS(d_msk)));
	MY_CUDA_CHECK( cudaMemcpy(d_msk+row/BITS(d_msk), &v, sizeof(*d_msk), cudaMemcpyHostToDevice) );

	MY_CUDA_CHECK( cudaMemcpy(d_prd+row, &pred, sizeof(pred), cudaMemcpyHostToDevice) );
	MY_CUDA_CHECK( cudaMemcpy(d_lvl+row, &level, sizeof(level), cudaMemcpyHostToDevice) );
	return;
}

void set_root_cuda(LOCINT root) {
	MY_CUDA_CHECK( cudaMemcpy(d_rbuf, &root, sizeof(*d_rbuf), cudaMemcpyHostToDevice) );
	return;
}

template<int BIT_VAL>
__global__ void bcount(const LOCINT *__restrict__ mask, int len, LOCINT *cnt) {

	const int tid = blockDim.x*blockIdx.x + threadIdx.x;

	if (tid < len) {
		LOCINT c, m = (BIT_VAL) ? mask[tid] : ~mask[tid];
		c = __popc(m);
#if LOCINT_SZ == 8
		c += __popc(m>>32);
#endif
		cnt[tid] = c;
	}
	return;
}

#if 0
__global__ void ormap(LOCINT *mask, int len, int n) {

	LOCINT m=0;
	const uint32_t tid = blockDim.x*blockIdx.x + threadIdx.x;

	if (tid < len) {
		m = mask[tid];
		for(int i = 1; i < n; i++)
			m |= mask[i*len + tid];
		mask[tid] = m;
	}
	return;
}
#else
__global__ void ormap(LOCINT *mask, int len, int n, const LOCINT *__restrict__ msk,  int *prd) {

	const uint32_t tid = blockDim.x*blockIdx.x + threadIdx.x;
	const LOCINT roff = dmycol*drow_bl + tid*BITS(msk);
	const int mval = msk[roff/BITS(msk)];
	LOCINT ow=0;

	if (tid < len) {
		mask += tid;
		for(int i = 0; i < n; i++) {
			LOCINT iw = mask[i*len] & (~ow);
			int b;
			while(b = __ffs(iw)) {
				b -= 1;
				const int z = 1<<b;
				if (!(mval&z)) {
					prd[roff + b] = -(1+i);
					ow |= z;
				}
				iw &= (~z);
			}
		}
		*mask = ow;
	}
	return;
}
#endif

template<int BIT_VAL>
__global__ void get_ind_list(const LOCINT *__restrict__ mask, int len, const LOCINT *__restrict__ cbuf, LOCINT *rbuf, LOCINT add) {

	const int	lid = threadIdx.x % warpSize;
	const int	tid = blockDim.x*blockIdx.x + threadIdx.x;
	const int	wid = tid/warpSize;

 	const int	mask1=(1<<lid);
 	const int	mask2=mask1-1;
 	const int	widx32=wid*32;
 	const LOCINT	base=wid*warpSize*LOCINT_SZ*8+lid;

	LOCINT	word, offs;

	if (tid < len) {
		word = BIT_VAL ? mask[tid] : ~mask[tid];
		offs = cbuf[tid];
	}

	#pragma unroll 32
	for(int i = 0; i < warpSize; i++) {

		if ((widx32 + i) >= len) break;

                const LOCINT o = __shfl(offs, i);
                LOCINT w = __shfl(word, i);
		LOCINT loc = 0;

		#pragma unroll 2
		for(int j = 0; j < (LOCINT_SZ/4); j++) {
			w >>= (32*j);
			if (w & mask1) {
				rbuf[o + loc + __popc(w & mask2)] = (LOCINT)(base + i*LOCINT_SZ*8 + j*LOCINT_SZ*4) + add;
			}
			loc += __popc(w);
		}
	}
	return;
}

LOCINT scan_col_csc_cuda(LOCINT *rbuf, LOCINT ld, int *rnum, int R, int C, LOCINT *sbuf, int *snum, int level, int inmask, int outmask) {

	int blocks;
	LOCINT	i, k;
	float	et=0;
	LOCINT	ncol=0, nrow=0;
#ifdef USE_THRUST
	static	thrust::device_ptr<LOCINT> d_val(d_cbuf);
#endif
#ifdef _TIMINGS_KERNEL
	TIMER_DEF(1);
	TIMER_DEF(2);
	TIMER_START(1);
#endif
	if (!outmask) {
		MY_CUDA_CHECK( cudaMemset(d_snum, 0, C*sizeof(*d_snum)) );
	} else {
		MY_CUDA_CHECK( cudaMemset(d_sbuf, 0, bmaplen*C*sizeof(*d_sbuf)) );
	}
	if (!inmask) {
		k = 0;
		ncol = 0;
		if (R > 1) {
			//...read data from host mem
			for(i = 0; i < R; i++) {
				if (rnum[i]) {
					MY_CUDA_CHECK( cudaMemcpyAsync(d_rbuf+ncol,
								       rbuf+i*ld,
								       rnum[i]*sizeof(*rbuf),
								       cudaMemcpyHostToDevice,
								       stream[k]) );
					read_edge_count<<<(rnum[i]+THREADS-1)/THREADS, THREADS, 0, stream[k]>>>(d_deg,
														d_rbuf+ncol,
														rnum[i],
														d_cbuf+ncol);
					k ^= 1;
					ncol += rnum[i];
				}
			}
		} else {
			//...data are supposed to be already on the device
			if (rnum[0]) {
				read_edge_count<<<(rnum[0]+THREADS-1)/THREADS, THREADS>>>(d_deg,
											  d_rbuf,
											  rnum[0],
											  d_cbuf+ncol);
				MY_CHECK_ERROR("read_edge_count");
			}
			ncol = rnum[0];
		}
	} else {
		if (R > 1) {
			//...read data from host mem
			MY_CUDA_CHECK( cudaMemcpy(d_fbmap, rbuf, bmaplen*R*sizeof(*rbuf), cudaMemcpyHostToDevice) );
		} else {
			//...data are supposed to be already on the device
			MY_CUDA_CHECK( cudaMemcpy(d_fbmap, d_rbuf, bmaplen*R*sizeof(*d_fbmap), cudaMemcpyDeviceToDevice) );
		}

		bcount<1><<<(bmaplen*R+THREADS-1)/THREADS, THREADS>>>(d_fbmap, bmaplen*R, d_cbuf);
		MY_CHECK_ERROR("bcount");

		ncol = __builtin_popcount(rbuf[bmaplen*R-1]); // check whether it works for icc...
#ifdef USE_THRUST
		thrust::exclusive_scan(d_val, d_val+bmaplen*R, d_val);
#else
		DeviceScan::ExclusiveSum(d_cubtmp, cubtmp_sz, d_cbuf, d_cbuf, bmaplen*R);
#endif
		MY_CUDA_CHECK( cudaMemcpy(&i, d_cbuf+bmaplen*R-1, sizeof(*d_cbuf), cudaMemcpyDeviceToHost) );
		ncol += i;

		if (ncol) {
			// kernel that creates the indices list...
			get_ind_list<1><<<(bmaplen*R+THREADS-1)/THREADS, THREADS>>>(d_fbmap, bmaplen*R, d_cbuf, d_rbuf, 0);
			MY_CHECK_ERROR("get_ind_list");
			read_edge_count<<<(ncol+THREADS-1)/THREADS, THREADS>>>(d_deg, d_rbuf, ncol, d_cbuf);
			MY_CHECK_ERROR("read_edge_count");
		}
	}
	if (ncol) {
		MY_CUDA_CHECK( cudaMemcpy(&i, d_cbuf+ncol-1, sizeof(*d_cbuf), cudaMemcpyDeviceToHost) );
		nrow = i;
#ifdef USE_THRUST
		thrust::exclusive_scan(d_val, d_val+ncol, d_val);
#else
		DeviceScan::ExclusiveSum(d_cubtmp, cubtmp_sz, d_cbuf, d_cbuf, ncol);
#endif
		MY_CUDA_CHECK( cudaMemcpy(&i, d_cbuf+ncol-1, sizeof(*d_cbuf), cudaMemcpyDeviceToHost) );
		nrow += i;
	} else {
		nrow = 0;
	}
#ifdef _TIMINGS_KERNEL
	TIMER_STOP(1);
#endif
	if (!nrow) goto out;

	MY_CUDA_CHECK( cudaEventRecord(start, 0) );
	blocks = (((nrow+ROWXTH-1)/ROWXTH)+THREADS-1)/THREADS;
	if (!outmask)
		scan_col<<<blocks, THREADS>>>(d_row, d_col, nrow,
					      d_rbuf, d_cbuf, ncol,
					      d_msk, d_prd, d_lvl, level,
					      d_sbuf, d_snum);
	else
		scan_col_bmap<<<blocks, THREADS>>>(d_row, d_col, nrow,
						   d_rbuf, d_cbuf, ncol,
						   d_msk, d_prd, d_lvl, level,
						   d_sbuf);
	MY_CUDA_CHECK( cudaEventRecord(stop, 0) );
	MY_CHECK_ERROR("scan_col");
	MY_CUDA_CHECK( cudaEventSynchronize(stop) );
	MY_CUDA_CHECK( cudaEventElapsedTime(&et, start, stop) );
	//printf("scan_col<<<%d, %d>>>(): %f ms\n", blocks, THREADS, et);
out:
#ifdef _TIMINGS_KERNEL
	TIMER_START(2);
#endif
	if (C > 1) {
		//...data need to be moved to host mem for MPI transfers
		if (!outmask) {
			MY_CUDA_CHECK( cudaMemcpy(snum, d_snum, C*sizeof(*snum), cudaMemcpyDeviceToHost) );
			for(i = 0; i < C; i++) {
				if (i == mycol) continue;
				if (snum[i])
					MY_CUDA_CHECK( cudaMemcpy(sbuf+i*ld,
								  d_sbuf+i*ld,
								  snum[i]*sizeof(*d_sbuf),
								  cudaMemcpyDeviceToHost) );
			}
			snum[mycol] = 0;//redundant
		} else {
			MY_CUDA_CHECK( cudaMemcpy(sbuf, d_sbuf, bmaplen*C*sizeof(*d_sbuf), cudaMemcpyDeviceToHost) );
		}
	}
#ifdef _TIMINGS_KERNEL
	TIMER_STOP(2);
	//prstat(ncol, "length of serach array:", 1);
	//prstat(((nrow+ROWXTH-1)/ROWXTH), "number of binsearch:", 1);
	prstat(TIMER_ELAPSED(1), "scan_col_csc_cuda 1 (us):", 0);
        prstat(et*1000.0, "scan_col_csc_cuda K (us):", 0);
        prstat(TIMER_ELAPSED(2), "scan_col_csc_cuda 2 (us):", 0);
#endif
	return 0;
}

__global__ void set_cmsk(LOCINT *inds, int n, LOCINT *msk) {

	const int tid = blockIdx.x*blockDim.x + threadIdx.x;
	if (tid < n) {
		//msk[inds[tid]] = 1;
		LOCINT i = inds[tid];
		atomicOr(msk + i/BITS(msk), ((LOCINT)1) << (i%BITS(msk)));
	}
	return;
}

#ifdef BU_SEARCH
LOCINT scan_col_csc_bu_cuda(LOCINT *rbuf, LOCINT ld, int *rnum, int R, int C, LOCINT *sbuf, int *snum, int level, int inmask, int outmask) {

	int blocks;
	LOCINT	i, k;
	float	et=0;
	LOCINT	nfrt=0, ncol=0, nrow=0;
#ifdef USE_THRUST
	static	thrust::device_ptr<LOCINT> d_val(d_cbuf);
#endif
#ifdef _TIMINGS_KERNEL
	TIMER_DEF(1);
	TIMER_DEF(2);
	TIMER_START(1);
#endif

	//assert(inmask == 0);
	//assert(outmask == 0);
	if (!outmask) {
		MY_CUDA_CHECK( cudaMemset(d_snum, 0, C*sizeof(*d_snum)) );
	} else {
		MY_CUDA_CHECK( cudaMemset(d_sbuf, 0, bmaplen*C*sizeof(*d_sbuf)) );
	}	

	nfrt = 0;
	if (!inmask) {
		if (R > 1) {
			//...read from host mem
			k = 0;
			// update current frontier's mask (d_rbuf used as temp)
			for(i = 0; i < R; i++) {
				if (rnum[i]) {
					MY_CUDA_CHECK( cudaMemcpyAsync(d_rbuf+nfrt,
								       rbuf+i*ld,
								       rnum[i]*sizeof(*rbuf),
								       cudaMemcpyHostToDevice,
								       stream[k]) );
					set_cmsk<<<(rnum[i]+THREADS-1)/THREADS, THREADS, 0, stream[k]>>>(d_rbuf+nfrt, rnum[i], d_cmsk);
					MY_CHECK_ERROR("set_cmsk");
					k ^= 1;
					nfrt += rnum[i];
				}
			}
			//set_cmsk<<<(nfrt+THREADS-1)/THREADS, THREADS>>>(d_rbuf, nfrt, d_cmsk);
			//MY_CHECK_ERROR("set_cmsk");
		} else {
			//...data are supposed to be already on the device
			if (rnum[0]) {
				set_cmsk<<<(rnum[0]+THREADS-1)/THREADS, THREADS>>>(d_rbuf, rnum[0], d_cmsk);
				MY_CHECK_ERROR("set_cmsk");
			}
			nfrt = rnum[0];
		}
	} else {
		if (R > 1) {
			//...read from host mem
			MY_CUDA_CHECK( cudaMemcpyAsync(d_cmsk, rbuf, bmaplen*R*sizeof(*rbuf), cudaMemcpyHostToDevice, stream[0]) );
		} else {
			//...data are supposed to be already on the device
			MY_CUDA_CHECK( cudaMemcpyAsync(d_cmsk, d_rbuf, bmaplen*R*sizeof(*d_cmsk), cudaMemcpyDeviceToDevice, stream[0]) );
		}
#if 0
		// just set nfrt != 0 if there is a 1 inthe bitmap
		for(i = 0; i < bmaplen*R; i++) {
			if (nfrt = __builtin_popcount(rbuf[i]))
				break;
		}
#else 
		nfrt = 1;
#endif
	}
	//printf("%s: nfrt=%"LOCPRI"\n", __func__, nfrt);

	if (nfrt) {
		// generate the list of unvisited rows
		bcount<0><<<(msk_len+THREADS-1)/THREADS, THREADS>>>(d_msk, msk_len, d_cbuf);
		MY_CHECK_ERROR("bcount_inv");

		DeviceScan::ExclusiveSum(d_cubtmp, cubtmp_sz, d_cbuf, d_cbuf, msk_len+1);
		MY_CUDA_CHECK( cudaMemcpy(&nrow, d_cbuf+msk_len, sizeof(nrow), cudaMemcpyDeviceToHost) );
		//printf("%s: nrow=%"LOCPRI"\n", __func__, nrow);

		if (nrow) {
			// kernel that creates the indices list...
			get_ind_list<0><<<(msk_len+THREADS-1)/THREADS, THREADS>>>(d_msk, msk_len, d_cbuf, d_rbuf, 0);
			MY_CHECK_ERROR("get_ind_list");
			read_edge_count<<<(nrow+THREADS-1)/THREADS, THREADS>>>(d_csr_deg, d_rbuf, nrow, d_cbuf);
			MY_CHECK_ERROR("read_edge_count");
			MY_CUDA_CHECK( DeviceScan::ExclusiveSum(d_cubtmp, cubtmp_sz, d_cbuf, d_cbuf, nrow+1) );
			MY_CUDA_CHECK( cudaMemcpy(&ncol, d_cbuf+nrow, sizeof(ncol), cudaMemcpyDeviceToHost) );
		} else {
			ncol = 0;
		}
		//fprintf(stderr, "%s: ncol=%"LOCPRI", nrow=%"LOCPRI"\n", __func__, ncol, nrow);
	} else {
		ncol = 0;
	}

#ifdef _TIMINGS_KERNEL
	TIMER_STOP(1);
#endif
	if (!ncol) goto out;

	MY_CUDA_CHECK( cudaEventRecord(start, 0) );
	blocks = (((ncol+ROWXTH-1)/ROWXTH)+THREADS-1)/THREADS;
	if (!outmask)
		scan_col_bu<<<blocks, THREADS>>>(d_csr_row, d_csr_col, ncol, 
					         d_rbuf, d_cbuf, nrow, d_cmsk,
					         d_msk, d_prd, d_lvl, level,
					         d_sbuf, d_snum);
	else
		scan_col_bmap_bu<<<blocks, THREADS>>>(d_csr_row, d_csr_col, ncol, 
						      d_rbuf, d_cbuf, nrow, d_cmsk,
						      d_msk, d_prd, d_lvl, level,
						      d_sbuf);

	MY_CUDA_CHECK( cudaEventRecord(stop, 0) );
	MY_CHECK_ERROR("scan_col");
	MY_CUDA_CHECK( cudaEventSynchronize(stop) );
	MY_CUDA_CHECK( cudaEventElapsedTime(&et, start, stop) );
	//printf("scan_col_%sbu<<<%d, %d>>>(): %f ms\n", outmask?"bmap_":"",blocks, THREADS, et);
out:
#ifdef _TIMINGS_KERNEL
	TIMER_START(2);
#endif
	if (C > 1) {
		if (!outmask) {
			MY_CUDA_CHECK( cudaMemcpy(snum, d_snum, C*sizeof(*snum), cudaMemcpyDeviceToHost) );
			for(i = 0; i < C; i++) {
				if (i == mycol) continue;
				if (snum[i])
					MY_CUDA_CHECK( cudaMemcpy(sbuf+i*ld,
								  d_sbuf+i*ld,
								  snum[i]*sizeof(*d_sbuf),
								  cudaMemcpyDeviceToHost) );
			}
			snum[mycol] = 0;//redundant
		} else {
			MY_CUDA_CHECK( cudaMemcpy(sbuf, d_sbuf, bmaplen*C*sizeof(*d_sbuf), cudaMemcpyDeviceToHost) );
		}
	}
#ifdef _TIMINGS_KERNEL
	TIMER_STOP(2);
	//prstat(ncol, "length of serach array:", 1);
	//prstat(((nrow+ROWXTH-1)/ROWXTH), "number of binsearch:", 1);
	prstat(TIMER_ELAPSED(1), "scan_col_csc_cuda 1 (us):", 0);
        prstat(et*1000.0, "scan_col_csc_cuda K (us):", 0);
        prstat(TIMER_ELAPSED(2), "scan_col_csc_cuda 2 (us):", 0);
#endif
	return 0;
}
#endif

LOCINT append_rows_cuda(LOCINT *rbuf, LOCINT ld, int *rnum, int R, int C, LOCINT *frt, int level, int iomask) {

	float	et=0;
	int	add;
#ifdef USE_THRUST
	static	thrust::device_ptr<LOCINT> d_val(d_cbuf);
#endif
#ifdef _TIMINGS_KERNEL
	TIMER_DEF(1);
	TIMER_DEF(2);
	TIMER_START(1);
#endif
	if (C > 1) {
		LOCINT	nrow=0;
		if (!iomask) {
			for(int i = 0; i < C; i++) {
				if (i == mycol) continue;
				if (rnum[i]) {
					MY_CUDA_CHECK( cudaMemcpy(d_rbuf+nrow, rbuf+i*ld, rnum[i]*sizeof(*rbuf), cudaMemcpyHostToDevice) );
					nrow += rnum[i];
				}
			}
			if (nrow) {
				// in-place prefix-sum of rnum (too small to bother thrust)
				int p, q;
				p = rnum[0]; rnum[0] = 0;
				for(int i = 1; i < C; i++) {
					q = rnum[i];
					rnum[i] = p + rnum[i-1];
					p = q;
				}
				MY_CUDA_CHECK( cudaMemcpy(d_cbuf, rnum, C*sizeof(*rnum), cudaMemcpyHostToDevice) );
			}
		} else {
			MY_CUDA_CHECK( cudaMemcpy(d_fbmap, rbuf, bmaplen*C*sizeof(*rbuf), cudaMemcpyHostToDevice) );

			// d_fbmap[0:bld-1] = OR(d_fbmap[i*bld:i*bld+bld-1], i=0,...C-1)
			//ormap<<<(bmaplen+THREADS-1)/THREADS, THREADS>>>(d_fbmap, bmaplen, C);
			ormap<<<(bmaplen+THREADS-1)/THREADS, THREADS>>>(d_fbmap, bmaplen, C, d_msk, d_prd);
			MY_CHECK_ERROR("ormap");

			bcount<1><<<(bmaplen+THREADS-1)/THREADS, THREADS>>>(d_fbmap, bmaplen, d_cbuf);
			MY_CHECK_ERROR("bcount");

			//MY_CUDA_CHECK( cudaMemcpy(&nrow, d_cbuf+bmaplen-1, sizeof(*d_cbuf), cudaMemcpyDeviceToHost) );
#ifdef USE_THRUST
			thrust::exclusive_scan(d_val, d_val+bmapleni+1, d_val);
#else
			DeviceScan::ExclusiveSum(d_cubtmp, cubtmp_sz, d_cbuf, d_cbuf, bmaplen+1);
#endif
			MY_CUDA_CHECK( cudaMemcpy(&/*add*/nrow, d_cbuf+bmaplen/*-1*/, sizeof(*d_cbuf), cudaMemcpyDeviceToHost) );
			//nrow += add;

			if (nrow) {
				// kernel that creates the indices list...
				get_ind_list<1><<<(bmaplen+THREADS-1)/THREADS, THREADS>>>(d_fbmap, bmaplen, d_cbuf, d_rbuf, mycol*row_bl);
				MY_CHECK_ERROR("get_ind_list");
			}
		}
#ifdef _TIMINGS_KERNEL
		TIMER_STOP(1);
#endif
		if (nrow) {
			MY_CUDA_CHECK( cudaEventRecord(start, 0) );
			if (!iomask)
				append_row<<<(nrow+THREADS-1)/THREADS, THREADS>>>(d_rbuf, nrow, d_cbuf, C, d_msk, d_prd,
										  d_lvl, level, d_sbuf+mycol*row_bl, d_snum+mycol);
			else
				append_row_bmap<<<(nrow+THREADS-1)/THREADS, THREADS>>>(d_rbuf, nrow, d_msk, d_prd,
										       d_lvl, level, d_sbuf+mycol*bmaplen);
			MY_CUDA_CHECK( cudaEventRecord(stop, 0) );
			MY_CHECK_ERROR("append_row");
			MY_CUDA_CHECK( cudaEventSynchronize(stop) );
			MY_CUDA_CHECK( cudaEventElapsedTime(&et, start, stop) );
		}
	}
#ifdef _TIMINGS_KERNEL
	TIMER_START(2);
#endif
	if (!iomask) {
		MY_CUDA_CHECK( cudaMemcpy(&add, d_snum+mycol, sizeof(add), cudaMemcpyDeviceToHost) );
		//add = front_add;
		if (add) {
			if (R > 1) {
				MY_CUDA_CHECK( cudaMemcpy(frt, d_sbuf+mycol*row_bl, add*sizeof(*d_sbuf), cudaMemcpyDeviceToHost) );
			} else {
				if (C > 1) {
					MY_CUDA_CHECK( cudaMemcpy(d_rbuf, d_sbuf+mycol*row_bl, add*sizeof(*d_sbuf), cudaMemcpyDeviceToDevice) );
				} else {
					LOCINT *__tmp = d_rbuf;
					d_rbuf = d_sbuf;
					d_sbuf = __tmp;
				}
			}
		}
	} else {
		bcount<1><<<(bmaplen+THREADS-1)/THREADS, THREADS>>>(d_sbuf+mycol*bmaplen, bmaplen, d_cbuf);
		MY_CHECK_ERROR("bcount");
#ifdef USE_THRUST
		add = thrust::reduce(d_val, d_val+bmaplen);
#else
		DeviceReduce::Sum(d_cubtmp, cubtmp_sz, d_cbuf, d_cbuf, bmaplen);
		LOCINT tmp;
		MY_CUDA_CHECK( cudaMemcpy(&tmp, d_cbuf, sizeof(tmp), cudaMemcpyDeviceToHost) );
		if (tmp > INT_MAX) {
			fprintf(stderr, "Toom many frontier elements for an int!\n");
			exit(EXIT_FAILURE);
		}
		add = tmp;
#endif
		if (R > 1) {
			MY_CUDA_CHECK( cudaMemcpy(frt, d_sbuf+mycol*bmaplen, bmaplen*sizeof(*d_sbuf), cudaMemcpyDeviceToHost) );
		} else {
			if (C > 1) {
				MY_CUDA_CHECK( cudaMemcpy(d_rbuf, d_sbuf+mycol*bmaplen, bmaplen*sizeof(*d_sbuf), cudaMemcpyDeviceToDevice) );
			} else {
				LOCINT *__tmp = d_rbuf;
				d_rbuf = d_sbuf;
				d_sbuf = __tmp;
			}
		}
	}
#ifdef _TIMINGS_KERNEL
	TIMER_STOP(2);
	prstat(TIMER_ELAPSED(1), "update_rows_cuda 1 (us):", 1);
        prstat(et*1000.0, "update_rows_cuda K (us):", 1);
        prstat(TIMER_ELAPSED(2), "update_rows_cuda 2 (us):", 1);
#endif
	return add;
}

__global__ void pred_req(const int *__restrict__ prd, LOCINT min, LOCINT max,
			 const LOCINT *__restrict__ msk, LOCINT *sbuf, int *snum) {

	LOCINT   m, r;
        const uint32_t tid = blockDim.x*blockIdx.x + threadIdx.x;

        if (tid >= (max-min)) return;
	r = tid + min;

        m = ((LOCINT)1) << (r%BITS(msk));
        if (!(msk[r/BITS(msk)]&m) || prd[r] >= 0) return;

	int p = -(prd[r]+1);
	int off = atomicAdd(snum+p, 1);
	sbuf[p*drow_bl + off] = r;

	return;
}

void pred_reqs_cuda(LOCINT min, LOCINT max,
		    LOCINT *sbuf, LOCINT ld, int *snum) {

	float et = 0;
	TIMER_DEF(0);
        TIMER_DEF(1);

	TIMER_START(0);
	MY_CUDA_CHECK( cudaMemset(d_snum, 0, C*sizeof(*d_snum)) );
	TIMER_STOP(0);

	MY_CUDA_CHECK( cudaEventRecord(start, 0) );
	pred_req<<<(max-min+THREADS-1)/THREADS, THREADS>>>(d_prd, min, max, d_msk, d_sbuf, d_snum);
	MY_CUDA_CHECK( cudaEventRecord(stop, 0) );
        MY_CUDA_CHECK( cudaEventSynchronize(stop) );
        MY_CUDA_CHECK( cudaEventElapsedTime(&et, start, stop) );

	TIMER_START(1);
	MY_CUDA_CHECK( cudaMemcpy(snum, d_snum, C*sizeof(*snum), cudaMemcpyDeviceToHost) );
	for(int i = 0; i < C; i++)
                MY_CUDA_CHECK( cudaMemcpy(sbuf+i*ld, d_sbuf+i*ld, snum[i]*sizeof(*d_sbuf), cudaMemcpyDeviceToHost) );
	TIMER_STOP(1);

	return;
}

__global__ void pred_resp(const int *__restrict__ prd, LOCINT *rbuf, LOCINT n) {

        const uint32_t tid = blockDim.x*blockIdx.x + threadIdx.x;

        if (tid >= n) return;
	rbuf[tid] = prd[rbuf[tid]];

	return;
}

void pred_resp_cuda(LOCINT *rbuf, LOCINT ld, int *rnum, LOCINT *sbuf, int *snum) {

	int nrow = 0;
	float et = 0;
	TIMER_DEF(0);
	TIMER_DEF(1);

	TIMER_START(0);
	for(int i = 0; i < C; i++) {
		if (rnum[i]) {
			MY_CUDA_CHECK( cudaMemcpy(d_rbuf+nrow, rbuf+i*ld, rnum[i]*sizeof(*rbuf), cudaMemcpyHostToDevice) );
			nrow += rnum[i];
		}
	}
	if (!nrow) return;
	TIMER_STOP(0);

	MY_CUDA_CHECK( cudaEventRecord(start, 0) );
	pred_resp<<<(nrow+THREADS-1)/THREADS, THREADS>>>(d_prd, d_rbuf, nrow);
	MY_CUDA_CHECK( cudaEventRecord(stop, 0) );
	MY_CUDA_CHECK( cudaEventSynchronize(stop) );
	MY_CUDA_CHECK( cudaEventElapsedTime(&et, start, stop) );

	TIMER_START(1);
	nrow = 0;
	for(int i = 0; i < C; i++) {
		if (rnum[i]) {
			MY_CUDA_CHECK( cudaMemcpy(sbuf+i*ld, d_rbuf+nrow, rnum[i]*sizeof(*d_rbuf), cudaMemcpyDeviceToHost) );
			nrow += rnum[i];
		}
		snum[i] = rnum[i];
	}
	TIMER_STOP(1);
	return;
}

void get_lvl(int *lvl) {
	MY_CUDA_CHECK( cudaMemcpy(lvl, d_lvl, row_pp*sizeof(*d_lvl), cudaMemcpyDeviceToHost) );
}

void get_prd(int *prd) {
	MY_CUDA_CHECK( cudaMemcpy(prd, d_prd, row_pp*sizeof(*d_prd), cudaMemcpyDeviceToHost) );
}

__global__ void msk_diff(LOCINT *zmsk, LOCINT *vmsk, LOCINT n, LOCINT *msk) {

	const int tid = blockDim.x*blockIdx.x + threadIdx.x;
	if (tid < n) {
		msk[tid] = vmsk[tid] & (~zmsk[tid]);
	}
	return;
}

void get_msk(LOCINT *msk) {
#ifdef BU_SEARCH
	LOCINT *d_tmp = (LOCINT *)CudaMallocSet(msk_len*sizeof(*d_tmp), 0);
	msk_diff<<<(row_pp+THREADS-1)/THREADS, THREADS>>>(d_zdmsk, d_msk, msk_len, d_tmp);
	MY_CUDA_CHECK( cudaMemcpy(msk, d_tmp, msk_len*sizeof(*msk), cudaMemcpyDeviceToHost) );
	MY_CUDA_CHECK(cudaFree(d_tmp));
#else
	MY_CUDA_CHECK( cudaMemcpy(msk, d_msk, ((row_pp+BITS(d_msk)-1)/BITS(d_msk))*sizeof(*d_msk), cudaMemcpyDeviceToHost) );
#endif
	return;
}

__global__ void get_zero_deg(LOCINT *deg, LOCINT n, LOCINT *msk) {

        const int tid = blockDim.x*blockIdx.x + threadIdx.x;
	if (tid < n) {
		if (deg[tid] == 0) atomicOr(msk + tid/BITS(msk), ((LOCINT)1) << (tid%BITS(msk)));
	}
	return;

}
__global__ void	set_isol_nodes(LOCINT *zmsk, LOCINT n, LOCINT *vmsk) {

        const int tid = blockDim.x*blockIdx.x + threadIdx.x;
	if (tid < n) {
		vmsk[tid] |= zmsk[tid];
	}
	return;
}

__global__ void inplace_unzip(uint64_t *v, uint64_t nhalf) {

	const int tid = blockIdx.x*blockDim.x + threadIdx.x;
	if (tid < nhalf) {
		uint64_t t = v[2*tid + 1];
		v[2*tid + 1] = v[2*(nhalf + tid)];
		v[2*(nhalf + tid)] = t;
	}
	return;
}

int sort_graph(uint64_t **ed, uint64_t ned) {

	void    *d_temp_storage=NULL;
	size_t  temp_storage_bytes=0;

	int	rv=1;
//	int	use_host_mem=0;

	uint64_t *ed_d=NULL;
	uint64_t *ed_sort_d=NULL;
	uint64_t ned_fix;

	ned_fix = ned+(ned%2);

	cudaError err;
	err = cudaMalloc(&ed_d, 2*ned_fix*sizeof(*ed_d));
	if(cudaSuccess != err) {
		rv = 0;
		goto out;
	}
	err = cudaMalloc(&ed_sort_d, 2*ned_fix*sizeof(*ed_sort_d));
	if(cudaSuccess != err) {
/*
		// if the second array does not fit in mem use mapped host memory...
		if (ned_fix != ned) {
			ed[0] = (uint64_t *)Realloc(ed[0], 2*ned_fix*sizeof(**ed));
		}
		err = cudaHostRegister(ed[0], 2*ned_fix*sizeof(**ed), cudaHostRegisterMapped);
		if(cudaSuccess != err) {
*/
			rv = 0;
			goto out;
/*
		}
		//printf("using mapped memory...");fflush(stdout);
		use_host_mem = 1;
		MY_CUDA_CHECK(cudaHostGetDevicePointer((void **)&ed_sort_d, ed[0], 0) );
*/
	}

	// last two ints won't be processed
	MY_CUDA_CHECK( cudaMemcpy(ed_d, ed[0], 2*ned*sizeof(*ed_d), cudaMemcpyHostToDevice) );

	inplace_unzip<<<(ned_fix/2+THREADS-1)/THREADS, THREADS>>>(ed_d, ned_fix/2); 

	MY_CUDA_CHECK(DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes,
						 ed_d,           ed_sort_d,
						 ed_d + ned_fix, ed_sort_d + ned_fix,
						 ned));
	err = cudaMalloc(&d_temp_storage, temp_storage_bytes);
	if(cudaSuccess != err) {
		rv = 0;
		goto out;
	}
	MY_CUDA_CHECK(DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes,
						 ed_d + ned_fix, ed_sort_d + ned_fix,
						 ed_d,           ed_sort_d,
						 ned));
	MY_CUDA_CHECK(DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes,
						 ed_sort_d,           ed_d,
						 ed_sort_d + ned_fix, ed_d + ned_fix,
						 ned));
	// inplace_unzip(inplace_unzip(v)) = inplace_zip(inplace_unzip(v))
	inplace_unzip<<<(ned_fix/2+THREADS-1)/THREADS, THREADS>>>(ed_d, ned_fix/2);
	MY_CUDA_CHECK( cudaMemcpy(ed[0], ed_d, 2*ned*sizeof(*ed_d), cudaMemcpyDeviceToHost) );
out:
	cudaGetLastError();
	if (ed_d) {
		MY_CUDA_CHECK( cudaFree(ed_d) )
	}
/*
	if (use_host_mem) {
		MY_CUDA_CHECK( cudaHostUnregister(ed[0]) );
	} else {
*/
		if (ed_sort_d) {
      			MY_CUDA_CHECK( cudaFree(ed_sort_d) );
		}
/*
	}
*/
	if (d_temp_storage) {
		MY_CUDA_CHECK( cudaFree(d_temp_storage) );
	}
	return rv;
}

#ifndef BU_SEARCH
void resetcuda_data(uint64_t ned, LOCINT *col, LOCINT *row) {
#else
void resetcuda_data(uint64_t ned, LOCINT *col, LOCINT *row, LOCINT *csr_col, LOCINT *csr_row, LOCINT *csr_gdg) {
#endif

	static int ftime=1;

	if (ftime) {
		MY_CUDA_CHECK( cudaMemcpy(d_col, col, (col_bl+1)*sizeof(*col), cudaMemcpyHostToDevice) );
		MY_CUDA_CHECK( cudaMemcpy(d_row, row, ned*sizeof(*row), cudaMemcpyHostToDevice) );
		set_degree<<<(col_bl+THREADS-1)/THREADS, THREADS>>>(d_col, d_deg, col_bl);
#ifdef BU_SEARCH
		MY_CUDA_CHECK( cudaMemcpy(d_csr_col, csr_col, ned*sizeof(*d_csr_col), cudaMemcpyHostToDevice) );
		MY_CUDA_CHECK( cudaMemcpy(d_csr_row, csr_row, (row_pp+1)*sizeof(*d_csr_row), cudaMemcpyHostToDevice) );
		set_degree<<<(row_pp+THREADS-1)/THREADS, THREADS>>>(d_csr_row, d_csr_deg, row_pp);
#endif
		ftime = 0;
	}
	MY_CUDA_CHECK( cudaMemset(d_lvl,-1, row_pp*sizeof(*d_lvl)) );
	MY_CUDA_CHECK( cudaMemset(d_prd, 0, row_pp*sizeof(*d_prd)) );
	MY_CUDA_CHECK( cudaMemset(d_msk, 0, ((row_pp+BITS(d_msk)-1)/BITS(d_msk))*sizeof(*d_msk)) );
#ifdef BU_SEARCH
	MY_CUDA_CHECK( cudaMemset(d_cmsk, 0, ((col_bl+BITS(d_msk)-1)/BITS(d_msk))*sizeof(*d_cmsk)) );
	
	// necessary to ignore isolated nodes when doing bottom-up steps
	LOCINT *d_tmp = (LOCINT *)CudaMallocSet(row_pp*sizeof(*d_tmp), 0);
	MY_CUDA_CHECK( cudaMemcpy(d_tmp, csr_gdg, row_pp*sizeof(*d_tmp), cudaMemcpyHostToDevice) );
	
	MY_CUDA_CHECK( cudaMemset(d_zdmsk, 0, ((row_pp+BITS(d_msk)-1)/BITS(d_msk))*sizeof(*d_zdmsk)) );
	get_zero_deg<<<(row_pp+THREADS-1)/THREADS, THREADS>>>(d_tmp, row_pp, d_zdmsk);

	set_isol_nodes<<<(msk_len+THREADS-1)/THREADS, THREADS>>>(d_zdmsk, msk_len, d_msk);

	MY_CUDA_CHECK( cudaDeviceSynchronize() );
	MY_CUDA_CHECK(cudaFree(d_tmp));
#endif
	return;
}

size_t initcuda_data(uint64_t ned) {

	d_col = (LOCINT *)CudaMallocSet((col_bl+1)*sizeof(*d_col), 0);
	d_row = (LOCINT *)CudaMallocSet(ned*sizeof(*d_row), 0);
	d_deg = (LOCINT *)CudaMallocSet(col_bl*sizeof(*d_deg), 0);
#ifdef BU_SEARCH
	d_csr_col = (LOCINT *)CudaMallocSet(ned*sizeof(*d_csr_col), 0);
	d_csr_row = (LOCINT *)CudaMallocSet((row_pp+1)*sizeof(*d_csr_row), 0);
	d_csr_deg = (LOCINT *)CudaMallocSet(row_pp*sizeof(*d_csr_deg), 0);
	d_cmsk = (LOCINT *)CudaMallocSet(((col_bl+BITS(d_msk)-1)/BITS(d_msk))*sizeof(*d_cmsk), 0);
#endif
	d_rbuf = (LOCINT *)CudaMallocSet(MAX(col_bl, row_pp)*sizeof(*d_rbuf), 0);

	size_t fbmap_len = MAX(R,C)*bmaplen;
	d_fbmap = (LOCINT *)CudaMallocSet(fbmap_len*sizeof(*d_fbmap), 0);
	size_t cbuf_len = MAX(col_bl, C);
	cbuf_len = MAX(cbuf_len, fbmap_len);
#ifdef BU_SEARCH
	cbuf_len = MAX(cbuf_len, row_pp);
#endif
	d_cbuf = (LOCINT *)CudaMallocSet((cbuf_len+1)*sizeof(*d_cbuf), 0);
	
	d_sbuf = (LOCINT *)CudaMallocSet(MAX(row_bl,row_pp)*sizeof(*d_sbuf), 0);
	d_snum = (int *)CudaMallocSet(C*sizeof(*d_snum), 0);

	msk_len = (row_pp+BITS(d_msk)-1)/BITS(d_msk);
	d_msk = (LOCINT *)CudaMallocSet(msk_len*sizeof(*d_msk), 0);
	d_lvl = (int *)CudaMallocSet(row_pp*sizeof(*d_lvl), -1);
	d_prd = (int *)CudaMallocSet(row_pp*sizeof(*d_prd), 0);
#ifdef BU_SEARCH
	// to store the bits corresponding to zero-degree vertices
	d_zdmsk = (LOCINT *)CudaMallocSet(msk_len*sizeof(*d_zdmsk), 0);
#endif

#ifndef USE_THRUST
	size_t bytes=0;
	
	DeviceScan::ExclusiveSum(NULL, bytes, d_cbuf, d_cbuf, cbuf_len);
	cubtmp_sz = bytes;
	
	DeviceReduce::Sum(NULL, bytes, d_cbuf, d_cbuf, bmaplen);
	cubtmp_sz = MAX(cubtmp_sz, bytes);
	
	d_cubtmp = (LOCINT *)CudaMallocSet(cubtmp_sz, 0);
#endif
/*
	if (0 == myid) {
		fprintf(stdout, "Memory for d_col: %zu\n", (col_bl+1)*sizeof(*d_col));
		fprintf(stdout, "Memory for d_row: %zu\n", ned*sizeof(*d_row));
		fprintf(stdout, "Memory for d_deg: %zu\n", col_bl*sizeof(*d_deg));
		fprintf(stdout, "Memory for d_rbuf: %zu\n", MAX(col_bl, row_pp)*sizeof(*d_rbuf));
		fprintf(stdout, "Memory for d_cbuf: %zu\n", MAX(col_bl, C)*sizeof(*d_cbuf));
		fprintf(stdout, "Memory for d_sbuf: %zu\n", MAX(row_bl,row_pp)*sizeof(*d_sbuf));
		fprintf(stdout, "Memory for d_snum: %zu\n", C*sizeof(*d_snum));
		fprintf(stdout, "Memory for d_msk: %zu\n", ((row_pp+BITS(d_msk)-1)/BITS(d_msk))*sizeof(*d_msk));
		fprintf(stdout, "Memory for d_lvl: %zu\n", row_pp*sizeof(*d_lvl));
		fprintf(stdout, "Memory for d_prd: %zu\n", row_pp*sizeof(*d_prd));
		fprintf(stdout, "Memory for d_fbmap: %zu\n", MAX(R,C)*bmaplen*sizeof(*d_fbmap));
#ifndef USE_THRUST
		fprintf(stdout, "Memory for d_cubtmp: %zu\n", cubtmp_sz);
#endif
	}
*/
	if (myid == 0) fprintf(stdout, "Kernels launch params: TXB=%d ROWXTH=%d\n", THREADS, ROWXTH);
	return tot_dev_mem;
}

void initcuda() {

	int dev;

	dev = assignDeviceToProcess();
	MY_CUDA_CHECK( cudaSetDevice(dev) );

	MY_CUDA_CHECK( cudaEventCreate(&start) );
        MY_CUDA_CHECK( cudaEventCreate(&stop) );

	MY_CUDA_CHECK( cudaStreamCreate(stream+0) );
        MY_CUDA_CHECK( cudaStreamCreate(stream+1) );

	MY_CUDA_CHECK( cudaMemcpyToSymbol(dN, &N, sizeof(dN),  0, cudaMemcpyHostToDevice) );
	MY_CUDA_CHECK( cudaMemcpyToSymbol(dC, &C, sizeof(dC),  0, cudaMemcpyHostToDevice) );
	MY_CUDA_CHECK( cudaMemcpyToSymbol(dR, &R, sizeof(dR),  0, cudaMemcpyHostToDevice) );
	MY_CUDA_CHECK( cudaMemcpyToSymbol(dmyrow, &myrow, sizeof(dmyrow),  0, cudaMemcpyHostToDevice) );
	MY_CUDA_CHECK( cudaMemcpyToSymbol(dmycol, &mycol, sizeof(dmycol),  0, cudaMemcpyHostToDevice) );
	MY_CUDA_CHECK( cudaMemcpyToSymbol(drow_bl, &row_bl, sizeof(drow_bl),  0, cudaMemcpyHostToDevice) );
	MY_CUDA_CHECK( cudaMemcpyToSymbol(dcol_bl, &col_bl, sizeof(dcol_bl),  0, cudaMemcpyHostToDevice) );
	MY_CUDA_CHECK( cudaMemcpyToSymbol(drow_pp, &row_pp, sizeof(drow_pp),  0, cudaMemcpyHostToDevice) );

	MY_CUDA_CHECK( cudaFuncSetCacheConfig(read_edge_count, cudaFuncCachePreferL1) );
	MY_CUDA_CHECK( cudaFuncSetCacheConfig(scan_col, cudaFuncCachePreferL1) );
#ifdef BU_SEARCH
	MY_CUDA_CHECK( cudaFuncSetCacheConfig(scan_col_bu, cudaFuncCachePreferL1) );
	MY_CUDA_CHECK( cudaFuncSetCacheConfig(scan_col_bmap_bu, cudaFuncCachePreferL1) );
#endif
	MY_CUDA_CHECK( cudaFuncSetCacheConfig(scan_col_bmap, cudaFuncCachePreferL1) );
	MY_CUDA_CHECK( cudaFuncSetCacheConfig(append_row, cudaFuncCachePreferL1) );
	MY_CUDA_CHECK( cudaFuncSetCacheConfig(append_row_bmap, cudaFuncCachePreferL1) );
	MY_CUDA_CHECK( cudaFuncSetCacheConfig(pred_req, cudaFuncCachePreferL1) );
	MY_CUDA_CHECK( cudaFuncSetCacheConfig(pred_resp, cudaFuncCachePreferL1) );
		
	MY_CUDA_CHECK( cudaFuncSetCacheConfig(bcount<0>, cudaFuncCachePreferL1) );
	MY_CUDA_CHECK( cudaFuncSetCacheConfig(bcount<1>, cudaFuncCachePreferL1) );
	MY_CUDA_CHECK( cudaFuncSetCacheConfig(get_ind_list<0>, cudaFuncCachePreferL1) );
	MY_CUDA_CHECK( cudaFuncSetCacheConfig(get_ind_list<1>, cudaFuncCachePreferL1) );
	MY_CUDA_CHECK( cudaFuncSetCacheConfig(ormap, cudaFuncCachePreferL1) );

	return;
}

void fincuda() {

	MY_CUDA_CHECK( cudaFree(d_col) );
	MY_CUDA_CHECK( cudaFree(d_deg) );
	MY_CUDA_CHECK( cudaFree(d_row) );
	MY_CUDA_CHECK( cudaFree(d_rbuf) );
	MY_CUDA_CHECK( cudaFree(d_cbuf) );
	MY_CUDA_CHECK( cudaFree(d_sbuf) );
	MY_CUDA_CHECK( cudaFree(d_snum) );
	MY_CUDA_CHECK( cudaFree(d_msk) );
	MY_CUDA_CHECK( cudaFree(d_lvl) );
	MY_CUDA_CHECK( cudaFree(d_prd) );
#ifdef BU_SEARCH
	MY_CUDA_CHECK( cudaFree(d_csr_col) );
	MY_CUDA_CHECK( cudaFree(d_csr_deg) );
	MY_CUDA_CHECK( cudaFree(d_csr_row) );
	MY_CUDA_CHECK( cudaFree(d_cmsk) );
	MY_CUDA_CHECK( cudaFree(d_zdmsk) );
#endif	
	MY_CUDA_CHECK( cudaFree(d_fbmap) );
#ifndef USE_THRUST
	MY_CUDA_CHECK( cudaFree(d_cubtmp) );
#endif
	MY_CUDA_CHECK( cudaEventDestroy(start) );
        MY_CUDA_CHECK( cudaEventDestroy(stop) );

	MY_CUDA_CHECK( cudaStreamDestroy(stream[0]) );
	MY_CUDA_CHECK( cudaStreamDestroy(stream[1]) );

	return;
}
