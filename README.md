## High performance, parallel, distributed code to perform breadth first searches on CUDA enabled GPUs

<img align="left" alt="bfs2D logo" height="140" width="200" src="img/bfs2D-logo-lower-space.png">

This is the official repository of bfs2D, a CUDA code to run BFSes on large graphs 
on GPU clusters with a peak performance of **5.5 Terateps** on 1024 GPUs and
**203.1** GTEPS on a single GPU.
Bfs2D was first developed 5 years ago to show that it is possible to implement a fast
parallel distributed code to perform a breadth first search on a cluster of GPUs. 
Several years later we are still improving our code because we like that it is both *fast* 
and **simple**. Bfs2D ensures a perfect load-balancing among threads by implementing the 
**active-edge parallelism technique** and the communications are reduced by means of
a 2D partitioning scheme. *Enjoy reading and using the bfs2D code!*


<br><br><br>
___
<br><br><br>

## [Table of contents](#table-of-contents)
+ [Performance Analysis](#performance-analysis)
+ [Installation Instructions](#how-to-install)
+ [References](#references)
+ [Credits](#credits)
+ [How it works](#how-it-works)
+ [Command lines](#command-lines)

<br><br><br>
___


## Performance Analysis
We measured the perfoemance of bfs2D on both synthetic and real world graphs.
Synthetic graphs were generated using the Graph500 RMAT generator.  Real-world
graphs have been taken from the
[University of Florida sparse matrix collection](http://www.cise.ufl.edu/research/sparse/matrices "University of Florida Collection")
and from the [Stanford Network Analysis Project](http://snap.stanford.edu/data "SNAP Collection").

All the outputs can be found in the **test_results** directory.

<br>

### Single-GPU results

We compared the performance of bfs2D with the latest version of
[Gunrock](http://gunrock.github.io/gunrock/doc/latest/index.html), a well known
framework for graph processing, and with
[Enterprise](https://github.com/iHeartGraph/Enterprise).  We ran the codes on
the same GPU, an Nvidia Titan X Pascal with 12GB of memory. The tests have been
run using the graphs made available with the Gunrock distribution.  For each
dataset, the mean number of GTEPS (billion of traversed edges per second) is
computed over 64 BFS operations started from 64 randomly selected root vertices.
In order to make the comparison as fair as possible, we used the same sequences
for both bfs2D and GUnrock. Enterprise does not allow to specify source nodes so
we report the results for the sequence generated by the code.  For what concerns
Gunrock, we used the command line options reported in the performance section of
the Gunrock web-site for the Pascal GPU. The command lines used for bfs2D are
available in the [Command lines](#command-lines) section. For Enterprise, we
generated the binary files it requires as input by converting the .mtx files
using the supplied utility (text_to_bin.bin).

Bfs2D expects input files to be in a simple edge list format. Each line
represents an edge and contains the two connected vertices.  The following
command can be used to quickly convert an input graph from matrix market
format:

``$ cat graph.mtx | grep -vE "^%" | tail -n +2 > ./graph.elist``


Please note that Gunrock always reports the results in term of **directed**
edges per second, even when the input graph is the ``--undirected`` option, as
in the cases below. The same applies to Entersprise. Bfs2D, on the other hand,
reports either directed or undirected TEPS based on the type of graph specified
in the command line. For this reason, the results of Bfs2D are reported in terms
of directed edges per second (for undirected graphs this is twice the number of
directed edges per second). The bfs2D-TD column shows the results obtained by
using only the *top down* method.  The bfs2D-DO column reports the results using
the *direction optimizing* optimization, which takes advantage of both *top
down*  and the *bottom up* approaches.

<br>

##### Performance comparison on some real-world datasets
|**Dataset**     |**Nvert** | **Nedge**|**Bfs2D-TD**| **Bfs2D-DO**| **Gunrock**  | **Enterprise**  
|----------------|---------:|---------:|:----------:|:-----------:|:------------:|:-------------:
|hollywood       |  1139905 |  57515616|     21.83  |    48.22    |     35.88    |      24.44    
|indochina       |  7414768 | 194109311|     19.55  |    26.26    |     12.37    |      10.19    
|soc-LiveJournal1|  4847571 |  68993773|     10.16  |    20.28    |     17.25    |       7.54    
|soc-orkut       |  2997166 | 106349210|     8.48   |    35.31    |     16.02    |      19.15    
|road-usa        | 23947347 |  28854312|     0.16   |    0.17     |     0.08     |       n.a.    

The performance is measured in GTEPS. Program outputs can be found in the
directory  test_results/TiXP/COMPARISON.

**Please, let us know if you have command lines arguments for Gunrock that
result in higher performance on the Titan X Pascal. 
<br><br><br>

The following table reports the results for a number of real-world graphs from
the SNAP collection. The results are in undirected GTEPS. Since BFS results are
often reported in terms of directed edges traversed per second, we also report
directed TEPS (within parenthesis).


| **Dataset**               |**Nvert** | **Nedge**|**GTEPS on K40**| **GTEPS on M40** |**GTPES on Titan X Pascal** | **GTPES on Tesla P100 PCIE-16GB** |
|---------------------------|---------:|---------:|---------------:|-----------------:|---------------------------:|----------------------------------:|
| as-skitter_internet_topo  | 1696415  | 11095298 | 1.94  (3.88)   |   2.40   (4.80)  |       4.53     (9.06)      |          3.47   (6.94)            |
| cage15                    | 5154859  | 99199551 | 2.49  (4.97)   |   3.76   (7.53)  |       6.07    (12.14)      |          6.16   (12.31)           |
| cit-Patents               | 3774768  | 16518948 | 1.02  (2.05)   |   1.32   (2.64)  |       2.44     (4.88)      |          2.26   (4.52)            |
| coAuthorsCiteseer         | 227320   | 814134   | 0.21  (0.43)   |   0.24   (0.49)  |       0.47     (0.94)      |          0.34   (0.68)            |
| coPapersCiteseer          | 434102   | 32073440 | 3.81  (7.63)   |   5.74   (11.49) |       10.21   (20.42)      |          8.52   (17.04)           |
| coPapersDBLP              | 540486   | 15245729 | 3.26  (6.52)   |   4.49   (8.99)  |       8.02    (16.04)      |          6.68   (13.36)           |
| com-orkut                 | 3072441  | 117185083| 6.29  (12.59)  |   9.22   (18.44) |       16.36   (32.72)      |          17.87  (35.73)           |
| delaunay_n20              | 1048576  | 3145686  | 0.06  (0.13)   |   0.08   (0.15)  |       0.14     (0.28)      |          0.10   (0.21)            |
| delaunay_n24              | 16777216 | 50331601 | 0.18  (0.37)   |   0.24   (0.48)  |       0.46     (0.92)      |          0.36   (0.72)            |
| hollywood-2009            | 1139905  | 57515616 | 7.94  (15.88)  |   12.23  (24.45) |       22.08   (44.16)      |          22.05  (44.10)           |
| mouse_gene                | 45101    | 14506196 | 5.12  (10.24)  |   7.84   (15.68) |       14.81   (29.62)      |          11.32  (22.65)           |
| roadNet-CA                | 1965206  | 2766607  | 0.03  (0.07)   |   0.04   (0.08)  |       0.08     (0.16)      |          0.05   (0.11)            |
| road_central              | 14081816 | 16933413 | 0.03  (0.06)   |   0.04   (0.07)  |       0.07     (0.14)      |          0.05   (0.10)            |
| soc-LiveJournal1          | 4847571  | 68993773 | 4.04  (8.07)   |   5.61   (11.21) |       10.10   (20.20)      |          9.67   (19.33)           |
| wb-edu                    | 9469213  | 57156537 | 1.77  (3.53)   |   2.38   (4.76)  |       4.17     (8.34)      |          3.86   (7.73)            |

<br><br>

##### Results for graphs from the 10th DIMACS Implementation Challenge:


| **Dataset**              |**GTEPS on K40**| **GTEPS on M40** | **GTPES on Titan X Pascal**  | **GTPES on Tesla P100 PCIE-16GB** |
|--------------------------|---------------:|-----------------:|-----------------------------:|----------------------------------:|
| kron_g500-simple-logn16  |  4.90   (9.80) | 6.23   (12.46)   |         11.48 (22.96)        |          8.38   (16.76)           | 
| kron_g500-simple-logn18  |  10.04  (20.08)| 13.57  (27.15)   |         25.33 (50.66)        |          23.10  (46.21)           | 
| kron_g500-simple-logn21  |  22.89  (45.77)| 26.96  (53.93)   |         54.35 (108.70)       |          58.63  (117.25)          |

<br><br>

#### Performance for RMAT graphs

The following tables report the performance (in GTEPS) of bfs2D for RMAT
graphs, varying the *scale* and the *edge factor* parameters, on four different
GPUs: K40, M40, Titan X Pascal and P100. For comparison purposes, in the last
column of the Titan X Pascal table (S19-TD) are reported the result using only
the Top Down method.  A single hyphen means that the run failed because the
graph didn't fit in the GPU memory.  For each configuration the performance is
given in both undirected and directed GTEPS (the latter within parenthesis).
The first column specify the value of the edge factor. The graphs have been 
generated using the generator provided by the Graph500 project.

<br>

###### Results on the Kepler K40:

   **EF**   |    **S=18**    |     **S=19**   |     **S=20**    |     **S=21**   |     **S=22**   |     **S=23**   |     **S=24**   |
:----------:|:--------------:|:--------------:|:---------------:|:--------------:|:--------------:|:--------------:|:--------------:|
     16     | 3.17   (6.35)  | 4.50   (8.99)  | 5.70   (11.40)  | 6.52   (13.03) | 6.78   (13.57) | 6.50   (12.99) |  4.50  (9.00)  |
     32     | 5.68   (11.35) | 7.70   (15.41) | 8.54   (17.07)  | 9.61   (19.22) | 9.45   (18.90) | 8.84   (17.67) |  6.81  (13.61) |
     64     | 10.14  (20.28) | 12.27  (24.54) | 11.74  (23.47)  | 15.67  (31.34) | 13.73  (27.45) | 12.52  (25.05) |        -       |
     128    | 19.09  (38.17) | 22.00  (44.00) | 20.74  (41.48)  | 22.34  (44.67) | 23.53  (47.07) |       -        |        -       |
     256    | 31.95  (63.89) | 41.77  (83.55) |**42.59 (85.17)**| 35.66  (71.32) |       -        |       -        |        -       |


###### Results on the Maxwell M40:

   **EF**   |    **S=18**    |     **S=19**   |      **S=20**    |     **S=21**    |     **S=22**   |     **S=23**   |     **S=24**   |
:----------:|:--------------:|:--------------:|:----------------:|:---------------:|:--------------:|:--------------:|:--------------:|
     16     | 3.92   (7.84)  | 5.70   (11.40) | 7.27   (14.54)   | 8.20   (16.40)  | 8.76   (17.52) | 9.28   (18.55) | 8.15  (16.31)  |
     32     | 7.16   (14.31) | 9.51   (19.03) | 10.81  (21.62)   | 10.52  (21.04)  | 11.91  (23.83) | 11.60  (23.20) | 10.75 (21.51)  |
     64     | 12.95  (25.90) | 16.38  (32.75) | 17.82  (35.65)   | 18.02  (36.04)  | 18.54  (37.09) | 18.88  (37.75) |       -        |
     128    | 24.71  (49.41) | 29.36  (58.73) | 33.41  (66.83)   | 31.47  (62.93)  | 26.83  (53.66) |        -       |       -        |
     256    | 40.64  (81.28) | 46.10  (92.19) |**57.67 (115.34)**| 53.76  (107.51) |        -       |        -       |       -        |


###### Results on the GeForce Titan X Pascal:

   **EF**   |    **S=18**    |     **S=19**      |     **S=20**   |     **S=21**    |     **S=22**   |     **S=23**   |     **S=24**   | |   **S=19 TD**  |
:----------:|:--------------:|:-----------------:|:--------------:|:---------------:|:--------------:|:--------------:|:--------------:|-|:--------------:|
     16     | 7.58  (15.16)  |   10.94 (21.88)   | 13.94 (27.88)  | 15.02 (30.04)   | 16.30 (32.60)  | 16.77 (33.54)  | 13.09 (26.18)  | | 6.70   (13.39) |    
     32     | 13.66 (27.32)  |   18.30 (36.60)   | 21.07 (42.14)  | 22.94 (45.88)   | 24.17 (48.34)  | 24.46 (48.92)  | 17.73 (35.46)  | | 9.32   (18.64) |
     64     | 23.46 (46.92)  |   29.62 (59.24)   | 32.99 (65.98)  | 34.12 (68.24)   | 39.18 (78.36)  | 36.29 (72.58)  |       -        | | 11.56  (23.12) |
     128    | 40.95 (81.90)  |   60.21 (120.42)  | 61.91 (123.82) | 54.38 (108.76)  | 50.06 (100.12) |       -        |       -        | | 13.63  (27.25) |
     256    | 85.63 (171.26) |**106.49 (212.98)**| 83.59 (167.18) | 105.82 (211.64) |       -        |       -        |       -        | | 18.72  (37.44) |


###### Results on the Pascal P100-PCIE-16GB:

   **EF**   |    **S=18**    |     **S=19**   |     **S=20**   |        **S=21**   |     **S=22**   |     **S=23**   |     **S=24**   |
:----------:|:--------------:|:--------------:|:--------------:|:-----------------:|:--------------:|:--------------:|:--------------:|
     16     | 5.92  (11.85)  | 9.64   (19.29) | 12.88 (25.75)  |   16.09 (32.18)   | 19.38 (38.75)  | 19.11  (38.21) | 18.44  (36.87) |
     32     | 11.41 (22.82)  | 16.55  (33.10) | 21.73 (43.46)  |   26.94 (53.89)   | 28.25 (56.51)  | 29.86  (59.72) | 26.89  (53.77) |
     64     | 20.31 (40.62)  | 27.57  (55.14) | 39.23 (78.45)  |   39.14 (78.27)   | 41.08 (82.16)  | 38.18  (76.36) |        -       |
     128    | 42.58 (85.17)  | 59.04  (118.07)| 57.73 (115.46) |   64.15 (128.29)  | 60.91 (121.82) |       -        |        -       |
     256    | 77.32 (154.64  | 108.59 (217.18)| 97.04 (194.07) |**126.12 (252.24)**|       -        |       -        |        -       |


<br><br><br>

### Multi-GPU results
The tests on distributed systems have been carried out on the Piz Daint cluster 
hosted at the Swiss Center for Scientific Computing (CSCS). Each compute node 
is equipped with an Intel Xeon E5-2690 (v3 @ 2.60GHz, 12 cores, 64GB RAM) 
and NVIDIA Tesla P100 (16GB of main memory)

The table below reports the weak scaling of bfs2D with up to 1024 P100 GPUs,
where it achieves a performance of **5.5 TeraTEPS**.

<br>

|**Nodes** |**S**|**EF**|      **GTEPS**    |
|:--------:|:---:|:----:|------------------:|
|   1      | 19  |  256 | 90.94   (181.89)  |
|   4      | 21  |  256 | 133.21  (266.41)  |
|   16     | 23  |  256 | 257.95  (515.91)  |
|   64     | 25  |  256 | 833.72  (1667.43) |
|   256    | 27  |  256 | 1640.86 (3281.73) |
|   1024   | 29  |  256 | 2779.78 (5559.55) |

<br><br><br>
___

## How to install
### Requirements
In order to build bfs2D you need an MPI library (OpenMPI, MPICH, MVAPICH, ...)
and CUDA installed on your machine. The code also depends on NVIDIA CUB library
and on the graph generator library provided by the reference code for the
Graph500 benchamrk, both provided in the source tree.


### Compile

In order to build bfs2D you need to:

* move into the "generator" directory and build the generator library:
  * $ cd generator/
  * Edit the Makefile.BFS.mpi setting the variable MPI_INC_DIR and MPICC to values matching your environment.
  * $ make -f Makefile.BFS.mpi
* Edit the Makefile in the top-level directory and change the variables
  * MPI_HOME and CUDA_HOME to suit your system.
* Build the binary:
  * $ make


### Run

At this point you should have a binary called "bfs2d" in your directory.

<PRE>


Usage:

bfs2d [general options] [file input options]
bfs2d [general options] [RMAT input options]

General options:
	-p &lt;R&gt;x&lt;C&gt;
		Specifies the size of the bidimensional grid processes  rearranged  as.
		The  value  of  R*C  must   match   the  total  number  of   processes.
                Default: 1x1.

	-N &lt;nbfs&gt;
		Specifies the number of BFSes performed on the input graph.
		Default: 1.

	-r &lt;root&gt;
		Specifies the vertex to be used as the root of the BFSes.
		Default: a random root will be generated for each BFS.

	-z &lt;root_seed&gt;
		Specifies the seed to be used to generate the sequence of root  vertices
		for  the  BFSes.   This  option  has  no  effect  if  -r  is  specified.
		Default: random value.

	-D
		Specifies that the graph must be considered directed.
		Default: undirected.

	-b [&lt;m&gt;-&lt;n&gt; | &lt;thres&gt;]
		Specifies, for each BFS, the levels  in  which  the  communications  are
		performed using a bitmap in place of a list of  vertices.   This  option
		accepts both an explicit range or a threshold.
		In the first case the bitmap is  used  for  communications  in  all  the
		levels between the specified extremes  (included).   A  range  of  '0-0'
		disables the use of the bitmap.
		If a threshold is specified, then the optimization is used for the three
		consecutive bfs levels following the first step in which a percentage of
		nodes greater than 'thres' is visited.  The threshold  must  be  in  the
		range [0.0, 1.0].
		Default: threshold, 0.000010.

	-u [&lt;m&gt;-&lt;n&gt; | &lt;thres&gt;]
		Specifies,  for  each  BFS,  the   levels   in   which   the   bottom-up
		expansion is used in place of the top-down.  This option accepts both an
		explicit range or a threshold.
		In the first case the bottom-up expansion is  used  in  all  the  levels
		between the specified extremes (included). A range of '0-0' disables the
		the bottom-up strategy.  
		If a threshold is specified, then the optimization is used for  the  two
		consecutive bfs levels following the first step in which a percentage of
		nodes greater than 'thres' is visited.  The threshold  must  be  in  the
		range [0.0, 1.0].
		Default: threshold, 0.020000.

	-o
		Enables the  dump of level and predecessor  informations to files.  Each
		process writes two files: lvl_RANK and prd_RANK.  The BFS  for which the
		data is dumped is always the last one (see -N option).
		Default: disabled.

	-x
		Enables  the  exhaustive  search  for  the  best  bitmap  and  bottom-up
		thresholds  to  be  used with the input graph.  When using this  option,
		possible  values  specified  with '-u'  and/or '-b' options are ignored.

RMAT input options:
	-S &lt;scale&gt;
		Specifies the scale the of R-MAT graph (2^scale vertices).
		Default value: 21.

	-E &lt;edge_factor&gt;
		Specifies the average number of edges  per  vertex  (2^scale*edge_factor
		total edges).
		Default value: 16.

	-d &lt;outfile&gt;
		Dump the generated graph into a file called 'outfile'.
		Default: graph not dumped.

	-s &lt;graph_seed&gt;
		Specifies the seed to be used to generate the graph.
		Default: 23.

File input options:
	-f &lt;graph_file&gt;
		Specifies the file containing the graph to visit. The file must
		be in a simple edge list format. Each line represents an edge 
		and contains the indices of the two connected vertices.
</PRE>

## Authors

**Mauro Bisson** (maurob@nvidia.com)

**Massimo Bernaschi** (massimo.bernaschi@cnr.it)

**Enrico Mastrostefano** (enrico.mastrostefano@uniroma1.it)

<br><br><br>
___

## References
Massimo Bernaschi, Mauro Bisson, Enrico Mastrostefano, Flavio Vella.
"Multilevel parallelism for the exploration of large-scale graphs"
Submitted to IEEE Transaction on Multi-Scale Computing Systems.

Mauro Bisson, Massimo Bernaschi, and Enrico Mastrostefano. 
["Parallel Distributed Breadth First Search on the Kepler Architecture"]
(http://ieeexplore.ieee.org/document/7234934/). 
In: IEEE Trans. Parallel Distrib. Syst. 27.7 (2016), pp. 2091–2102. 
(doi: 10.1109/ TPDS.2015.2475270). 
Preprint available at arxiv: https://arxiv.org/abs/1408.1605

Enrico Mastrostefano and Massimo Bernaschi. 
["Efficient breadth first search on multi-GPU systems"]
(http://www.sciencedirect.com/science/article/pii/S0743731513001135).
In: J. Parallel Distrib. Comput. 73.9 (2013), pp. 1292–1305. 
(doi: 10.1016/j.jpdc.2013.05.007). 
Part of this paper is in the [PhD Dissertation]
(https://www.google.it/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwi8l77egt7TAhVG1xoKHSkqBeMQFggnMAA&url=http%3A%2F%2Fpadis.uniroma1.it%2Fbitstream%2F10805%2F2120%2F1%2FPhdT_Mastrostefano.pdf&usg=AFQjCNHFVHOHDmhv7rQDy0BhY_9Wbe0xgg&sig2=Qq9A0JHnBuMHhk45_jFbjg)
of Enrico Mastrostefano.

Enrico Mastrostefano. "Large graphs on multi-Gpus". Gpu Technology Conference 2012.
The full talk is available here: http://nvidia.fullviewmedia.com/gtc2012/0516-3-S0241.html
and the pdf version here: http://developer.download.nvidia.com/GTC/PDF/GTC2012/PresentationPDF/S0241-GTC2012-Graph-Multi-GPUs.pdf

<br><br><br>
___

## How it works
A (possibly) short description of the algorithms and techniques used in the code.
TODO

<br><br><br>
___

## Command lines
Command lines used to run the tests reported in this page.

### Command lines for real world graphs
### Command lines for synthetic graphs

